﻿using System;
using System.Linq;

namespace Iodynis.Libraries.Parsing.Example.Calculator
{
    public static class Program
    {
        private static string Debug = "";

        public static void Main()
        {
            Lexer lexer = new Lexer(LexerMode.MATCH_FIRST)
                .String(CalculatorToken.NEWLINE, "\r\n", "\n")
                .String(CalculatorToken.SPACE, " ", "\t")

                .String(CalculatorToken.BRA, "(")
                .String(CalculatorToken.KET, ")")
                .String(CalculatorToken.PLUS, "+")
                .String(CalculatorToken.MINUS, "-")
                .String(CalculatorToken.DIVISION, "/")
                .String(CalculatorToken.MULTIPLY, "*")
                .String(CalculatorToken.POWER, "^")
                .String(CalculatorToken.REMAINDER, "%")

                .Regex(CalculatorToken.STRING, @"([a-zA-Z]+)")
                .Regex(CalculatorToken.NUMBER, @"([0-9]+(?>\.[0-9]+)?)");

            Syntaxer syntaxer = new Syntaxer()
                .Remove(CalculatorToken.NEWLINE)
                .Remove(CalculatorToken.SPACE)

                .Group(CalculatorToken.BRACKET, CalculatorToken.BRA, CalculatorToken.KET)
                .AddPass()
                .Function(CalculatorToken.FUNCTION, CalculatorToken.ARGUMENT, CalculatorToken.STRING, CalculatorToken.BRACKET, CalculatorToken.NONE)
                .AddPass()
                .Binary(CalculatorToken.POWER)
                .AddPass()
                .Binary(CalculatorToken.MULTIPLY)
                .Binary(CalculatorToken.DIVISION)
                .AddPass()
                .Binary(CalculatorToken.PLUS)
                .Binary(CalculatorToken.MINUS)
                .AddPass()
                .Binary(CalculatorToken.REMAINDER);

            Interpretator interpretator = new Interpretator()
                // Operators
                .Simple(CalculatorToken.NUMBER,    (_symbol, _arguments) => Double.Parse(_symbol))
                .Simple(CalculatorToken.PLUS,      (_symbol, _arguments) => (double)_arguments[0] + (double)_arguments[1])
                .Simple(CalculatorToken.MINUS,     (_symbol, _arguments) => (double)_arguments[0] - (double)_arguments[1])
                .Simple(CalculatorToken.MULTIPLY,  (_symbol, _arguments) => (double)_arguments[0] * (double)_arguments[1])
                .Simple(CalculatorToken.DIVISION,  (_symbol, _arguments) => (double)_arguments[0] / (double)_arguments[1])
                .Simple(CalculatorToken.REMAINDER, (_symbol, _arguments) => (double)_arguments[0] % (double)_arguments[1])
                .Simple(CalculatorToken.POWER,     (_symbol, _arguments) => Math.Pow((double)_arguments[0], (double)_arguments[1]))
                // Groups
                .Simple(CalculatorToken.BRACKET,   (_symbol, _arguments) => _arguments[0])
                .Simple(CalculatorToken.ARGUMENT,  (_symbol, _arguments) => _arguments[0])
                // Constants
                .Simple(CalculatorToken.STRING, (_symbol, _arguments) =>
                {
                    switch (_symbol.ToUpper())
                    {
                        case "PI":
                            return Math.PI;
                        case "E":
                            return Math.E;
                        default:
                            throw new Exception($"Symbol {_symbol} is not supported.");
                    }
                })
                // Functions
                .Simple(CalculatorToken.FUNCTION, (_symbol, _arguments) =>
                {
                    int count = _arguments.Count();
                    if (count != 1)
                    {
                        throw new Exception($"Function has {count} arguments.");
                    }
                    double argument = Double.Parse(_arguments[0].ToString());

                    switch (_symbol.ToUpper())
                    {
                        case "SQRT":
                            return Math.Sqrt(argument);
                        case "SIN":
                            return Math.Sin(argument);
                        case "COS":
                            return Math.Cos(argument);
                        case "TAN":
                            return Math.Tan(argument);
                        case "COTAN":
                            return 1 / Math.Tan(argument);
                        default:
                            throw new Exception($"Function {_symbol} is not supported.");
                    }
                });

            Parser parser = new Parser(lexer, syntaxer, interpretator);
            parser.DebugEvent += OnDebugEvent;

            while (true)
            {
                Console.Write("> ");
                try
                {
                    string expression = Console.ReadLine();
                    object @object = parser.Parse(expression);

                    Console.SetCursorPosition(expression.Length + 2, Console.CursorTop - 1);
                    Console.Write(" = ");
                    Console.WriteLine(@object?.ToString());
                    Console.WriteLine();
                    Console.Write(Debug);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
                Console.WriteLine();
            }
        }

        private static void OnDebugEvent(object sender, ParserDebugEventArguments arguments)
        {
            // Show only at successfull interpreter pass
            if (arguments.StageType == ParserDebugEventArgumentStageType.INTERPRETER_PASS_AFTER_SUCCESS)
            {
                Parser parser = sender as Parser;
                Debug = parser.TokenManager.PrintSimple(parser, 16);
            }
        }
    }
}
