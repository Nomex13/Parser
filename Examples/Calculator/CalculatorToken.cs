﻿namespace Iodynis.Libraries.Parsing.Example.Calculator
{
    public enum CalculatorToken : int
    {
		ROOT,
		NONE,

		SPACE,
		NEWLINE,

        NUMBER,
        STRING,

        FUNCTION,
        ARGUMENT,
		BRACKET,
		BRA,
		KET,

		PLUS,
		MINUS,
		MULTIPLY,
		DIVISION,
        POWER,
        REMAINDER,
	}
}
