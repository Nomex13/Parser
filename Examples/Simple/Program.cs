﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Parsing.Example.Simple
{
    public static class Program
    {
        public static void Main(params string[] arguments)
        {
            Lexer lexer = new Lexer(LexerMode.MATCH_FIRST)
                .StringBlock(SimpleToken.COMMENT_XML, "///", false, "\r\n", true)

                .StringBlock(SimpleToken.COMMENT, "#", false, "\r\n", false)
                .StringBlock(SimpleToken.COMMENT, "//", false, "\r\n", false)
                .StringBlock(SimpleToken.COMMENT, "/*", false, "*/", false)

                .StringBlock(SimpleToken.ANNOTATION, "@", false, "\r\n", false)

                .String(SimpleToken.NEWLINE, "\r\n", "\n")
                .String(SimpleToken.SPACE, " ", "\t")

                .String(SimpleToken.BRA_ROUND, "(")
                .String(SimpleToken.KET_ROUND, ")")
                .String(SimpleToken.BRA_SQUARE, "[")
                .String(SimpleToken.KET_SQUARE, "]")
                .String(SimpleToken.COMMA, ",")
                .String(SimpleToken.EXCLAMATION, "!")
                .String(SimpleToken.AMPERSAND, "&")
                .String(SimpleToken.BAR, "|")
                .String(SimpleToken.PLUS, "+")
                .String(SimpleToken.MINUS, "-")
                .String(SimpleToken.DIVISION, "/")
                .String(SimpleToken.MULTIPLY, "*")
                .String(SimpleToken.EQUAL, "=")

                .Regex(SimpleToken.CHARACTER, @"'([^\\']|(?:\\')|(?:\\0)|(?:\\a)|(?:\\b)|(?:\\f)|(?:\\v)|(?:\\t)|(?:\\r)|(?:\\n)|(?:\\x[0-9a-fA-F]{1,4})|(?:\\u[0-9a-fA-F]{4})|(?:\\U[0-9a-fA-F]{8}))'")
                .StringBlockEscaped(SimpleToken.STRING, "\"", false, "\"", false, new Dictionary<string, string>()
                {
                    { "\\\\", "\\" },
                    { "\\\"", "\"" },
                    { "\\t", "\t" },
                    { "\\r", "\r" },
                    { "\\n", "\n" },
                    { "\\0", "\0" },
                    { "\\v", "\v" },
                    { "\\a", "\a" },
                    { "\\b", "\b" },
                    { "\\f", "\f" },
                })

                .Regex(SimpleToken.NUMBER_IN_UNITS, @"([1-9][0-9]*)([a-zA-Z]+)", SimpleToken.NUMBER_REAL, SimpleToken.UNITS)
                .Regex(SimpleToken.LITERAL,    @"[a-zA-Z_]+")
                .Regex(SimpleToken.NUMBER_REAL, @"[0-9]+\.[0-9]+")
                .Regex(SimpleToken.NUMBER_INTEGER, @"[0-9]+")
                .Regex(SimpleToken.NUMBER_HEX, @"0x[0-9]+")
                .Regex(SimpleToken.NUMBER_OCT, @"0[0-9]+")
                .Regex(SimpleToken.NUMBER_BIN, @"0b[0-9]+")

                .AddPass(SimpleToken.ANNOTATION)

                .String(SimpleToken.SPACE, " ", "\t")
                .Regex(SimpleToken.LITERAL, @"[a-zA-Z_]+")
                .Regex(SimpleToken.NUMBER_REAL, @"[0-9]+\.[0-9]+")
                .Regex(SimpleToken.NUMBER_INTEGER, @"[0-9]+")
                .String(SimpleToken.EXCLAMATION, "!")
                ;

            Lexer lexerXml = new Lexer()
                .AddPass(SimpleToken.COMMENT_XML)

                .String(SimpleToken.SPACE, " ", "\t")
                .String(SimpleToken.NEWLINE, "\r\n", "\n")
                .Regex(SimpleToken.LITERAL, @"[a-zA-Z_]+")
                .Regex(SimpleToken.COMMENT_XML_TAG, @"(?<=\<\s)([a-zA-Z]+)(?=\s\/\>)")
                .Regex(SimpleToken.COMMENT_XML_TAG_OPENING, @"(?<=\<\s)([a-zA-Z]+)(?=\s\/\>)")
                .Regex(SimpleToken.COMMENT_XML_TAG_CLOSING, @"(?<=\<\s\/\s)([a-zA-Z]+)(?=\s\/\>)");

            Syntaxer syntaxer = new Syntaxer()
                .AddPass("XML")
                .Merge(SimpleToken.COMMENT_XML)
                .AddPass("XML Lexer")
                .Lex(lexerXml)
                .AddPass("Comments and spaces")
                .Remove(SimpleToken.COMMENT)
                .Remove(SimpleToken.NEWLINE)
                .Remove(SimpleToken.SPACE)
                .AddPass("Brackets")
                .Group(SimpleToken.BRACKET_ROUND, SimpleToken.BRA_ROUND, SimpleToken.KET_ROUND)
                .Group(SimpleToken.BRACKET_SQUARE, SimpleToken.BRA_SQUARE, SimpleToken.KET_SQUARE)
                .AddPass("Functions")
                .Function(ComplexToken.FUNCTION, ComplexToken.ARGUMENT, SimpleToken.LITERAL, SimpleToken.BRACKET_ROUND, SimpleToken.COMMA)
                .AddPass("Operators")
                .UnaryLeft(SimpleToken.EXCLAMATION)
                .AddPass("Operators")
                .Binary(ComplexToken.MULTIPLY, SimpleToken.MULTIPLY)
                .Binary(ComplexToken.DIVIDE, SimpleToken.DIVISION)
                .AddPass("Operators")
                .Binary(ComplexToken.PLUS, SimpleToken.PLUS)
                .UnaryLeft(ComplexToken.PREPLUS, SimpleToken.PLUS)
                .UnaryRight(ComplexToken.POSTPLUS, SimpleToken.PLUS)
                .Binary(ComplexToken.MINUS, SimpleToken.MINUS)
                .UnaryLeft(ComplexToken.PREMINUS, SimpleToken.MINUS)
                .UnaryRight(ComplexToken.POSTMINUS, SimpleToken.MINUS)
                .AddPass("Operators")
                .Binary(ComplexToken.ASSIGN, SimpleToken.EQUAL)
                ;

            Interpretator interpretator = new Interpretator()
                .Simple(SimpleToken.LITERAL, (_symbol, _arguments) =>
                {
                    return _symbol;
                })
                .Simple(SimpleToken.NUMBER_INTEGER, (_symbol, _arguments) =>
                {
                    return Int32.Parse(_symbol);
                })
                .Simple(SimpleToken.NUMBER_REAL, (_symbol, _arguments) =>
                {
                    return Double.Parse(_symbol);
                })
                .Simple(ComplexToken.ARGUMENT, (_symbol, _arguments) =>
                {
                    int count = _arguments.Count();
                    if (count == 0)
                    {
                        return null;
                    }
                    if (count == 1)
                    {
                        return _arguments[0];
                    }
                    throw new Exception($"Argument token supports only 0 or 1 argument.");
                })
                ;

            Parser parser = new Parser(lexer, syntaxer, interpretator);
            parser.DebugEvent += OnDebugEvent;
            string text = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test.txt"), Encoding.UTF8) + "\r\n";
            object @object = parser.Parse(text);
        }

        private static void OnDebugEvent(object sender, ParserDebugEventArguments arguments)
        {
            if (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.BEFORE))
            {
                return;
            }

            Parser parser = sender as Parser;

            string file = "debug." +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.LEXER_PASS) ? "lexer." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.SYNTAXER_PASS) ? "syntaxer." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.INTERPRETER_PASS) ? "interpreter." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.AFTER) ? "after." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.BEFORE) ? "before." : "") +
                (arguments.PassIndex >= 0 ? ("pass #" + arguments.PassIndex + ".") : "") +
                (String.IsNullOrEmpty(arguments.StageName) ? "" : (arguments.StageName.ToLower() + ".") ) +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.SUCCESS) ? "success." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.FAILURE) ? "failure." : "") +
                "txt";
            File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file), parser.TokenManager.PrintSimple(parser, 64), Encoding.UTF8);
        }
    }
	public enum ComplexToken : int
    {
        FUNCTION,
        ARGUMENT,

        FUNCTION_AND,
        FUNCTION_OR,
        FUNCTION_NOT,
        FUNCTION_RISK,
        FUNCTION_K_IN_N,
        FUNCTION_ENGINE,

        ASSIGN,
        EQUAL,
        MULTIPLY,
        DIVIDE,
        PLUS,
        PREPLUS,
        POSTPLUS,
        OPERATOR_NOT,
        MINUS,
        PREMINUS,
        POSTMINUS,
    }
	public enum SimpleToken : int
	{
		ROOT = 1,
		COMMENT,
		COMMENT_XML,
		RETURN,
		NEWLINE,

		ANNOTATION,
		ANNOTATION_NAME,
		ANNOTATION_ARGUMENT,

        COMMENT_XML_TAG,
        COMMENT_XML_TAG_OPENING,
        COMMENT_XML_TAG_CLOSING,

        //OPERATOR_BRACKET_ROUND,
        //OPERATOR_BRACKET_SQUARE,
        //OPERATOR_ASSIGN,
        //OPERATOR_AND,
        //OPERATOR_OR,
        //OPERATOR_EXCLAMATION,

		BRACKET_SQUARE,
		BRA_SQUARE,
		KET_SQUARE,
		BRACKET_ROUND,
		BRA_ROUND,
		KET_ROUND,
		BRACKET_CURLY,
		BRA_CURLY,
		KET_CURLY,
		BRACKET_SHARP,
		BRA_SHARP,
		KET_SHARP,
		BRA_0,
		KET_0,
		BRACKET_0,
		BRA_1,
		KET_1,
		BRACKET_1,
		BRA_2,
		KET_2,
		BRACKET_2,
		BRA_3,
		KET_3,
		BRACKET_3,
		BRA_4,
		KET_4,
		BRACKET_4,
		BRA_5,
		KET_5,
		BRACKET_5,
		BRA_6,
		KET_6,
		BRACKET_6,
		BRA_7,
		KET_7,
		BRACKET_7,
		BRA_8,
		KET_8,
		BRACKET_8,
		BRA_9,
		KET_9,
		BRACKET_9,

		TAB,
		SPACE,
		LETTER,
		DIGIT,
		STRING,
		CHARACTER,
		NUMBER_INTEGER,
		NUMBER_REAL,
		NUMBER_IN_UNITS,
		UNITS,
		NUMBER_HEX,
		NUMBER_OCT,
		NUMBER_DEC,
		NUMBER_BIN,
		INTEGER,
		REAL,
		TIME,
		DATE,

		WORD,
		VALUE,
		NAME,
		LITERAL,

		AND,
		OR,
		NOT,
		XOR,
		INCREMENT,
		DECREMENT,

		PLUS,
		MINUS,
		MULTIPLY,
		DIVISION,

		ASTERISK,
		SLASH,
		EQUAL,
		NOT_EQUAL,
		LESS,
		LESS_OR_EQUAL,
		NOT_LESS,
		GREATER,
		GREATER_OR_EQUAL,
		NOT_GREATER,
		ASSIGN,

		EXCLAMATION,
		QUESTION,
		COMMA,
		DOT,
		PERIOD,
		DASH,
		SINGLE_QUOTE,
		DOUBLE_QUOTE,
		COLON,
		SEMICOLON,

		UNDERLINE,
		UNDERSCORE,
		AT,
		SHARP,
		DOLLAR,
		PERCENTAGE,
		CIRCUMFLEX,
		BAR,
		AMPERSAND,
		BACKSLASH,
		GRAVE,
		TILDE,

		CUSTOM_0,
		CUSTOM_1,
		CUSTOM_2,
		CUSTOM_3,
		CUSTOM_4,
		CUSTOM_5,
		CUSTOM_6,
		CUSTOM_7,
		CUSTOM_8,
		CUSTOM_9,

		ONE,
		TWO,
		THREE,
		MULTIPLE,
		ANY,
		UNKNOWN = 0
	}
}
