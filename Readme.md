# Parser

Library to parse text and interpret it.

## Usage

Define tokens:

```csharp
public enum CalculatorToken : int
{
	ROOT,
	NONE,

	SPACE,
	NEWLINE,

	NUMBER,
	STRING,

	FUNCTION,
	ARGUMENT,
	BRACKET,
	BRA,
	KET,

	PLUS,
	MINUS,
	MULTIPLY,
	DIVISION,
	POWER,
	REMAINDER,
}
```

Create and setup the lexer:

```csharp
Lexer lexer = new Lexer(LexerMode.MATCH_FIRST)
	.String(CalculatorToken.NEWLINE, "\r\n", "\n")
	.String(CalculatorToken.SPACE, " ", "\t")

	.String(CalculatorToken.BRA, "(")
	.String(CalculatorToken.KET, ")")
	.String(CalculatorToken.PLUS, "+")
	.String(CalculatorToken.MINUS, "-")
	.String(CalculatorToken.DIVISION, "/")
	.String(CalculatorToken.MULTIPLY, "*")
	.String(CalculatorToken.POWER, "^")
	.String(CalculatorToken.REMAINDER, "%")

	.Regex(CalculatorToken.STRING, @"([a-zA-Z]+)")
	.Regex(CalculatorToken.NUMBER, @"([0-9]+(?>\.[0-9]+)?)");
```

Create and setup the syntax analyzer:

```csharp
Syntaxer syntaxer = new Syntaxer()
	.Remove(CalculatorToken.NEWLINE)
	.Remove(CalculatorToken.SPACE)

	.Group(CalculatorToken.BRACKET, CalculatorToken.BRA, CalculatorToken.KET)
	.AddPass()
	.Function(CalculatorToken.FUNCTION, CalculatorToken.ARGUMENT, CalculatorToken.STRING, CalculatorToken.BRACKET, CalculatorToken.NONE)
	.AddPass()
	.Binary(CalculatorToken.POWER)
	.AddPass()
	.Binary(CalculatorToken.MULTIPLY)
	.Binary(CalculatorToken.DIVISION)
	.AddPass()
	.Binary(CalculatorToken.PLUS)
	.Binary(CalculatorToken.MINUS)
	.AddPass()
	.Binary(CalculatorToken.REMAINDER);
```

Create and setup the interpretator:

```csharp
Interpretator interpretator = new Interpretator()
	// Operators
	.Simple(CalculatorToken.NUMBER,    (_symbol, _arguments) => Double.Parse(_symbol))
	.Simple(CalculatorToken.PLUS,      (_symbol, _arguments) => (double)_arguments[0] + (double)_arguments[1])
	.Simple(CalculatorToken.MINUS,     (_symbol, _arguments) => (double)_arguments[0] - (double)_arguments[1])
	.Simple(CalculatorToken.MULTIPLY,  (_symbol, _arguments) => (double)_arguments[0] * (double)_arguments[1])
	.Simple(CalculatorToken.DIVISION,  (_symbol, _arguments) => (double)_arguments[0] / (double)_arguments[1])
	.Simple(CalculatorToken.REMAINDER, (_symbol, _arguments) => (double)_arguments[0] % (double)_arguments[1])
	.Simple(CalculatorToken.POWER,     (_symbol, _arguments) => Math.Pow((double)_arguments[0], (double)_arguments[1]))
	// Groups
	.Simple(CalculatorToken.BRACKET,   (_symbol, _arguments) => _arguments[0])
	.Simple(CalculatorToken.ARGUMENT,  (_symbol, _arguments) => _arguments[0])
	// Constants
	.Simple(CalculatorToken.STRING, (_symbol, _arguments) =>
	{
		switch (_symbol.ToUpper())
		{
			case "PI":
				return Math.PI;
			case "E":
				return Math.E;
			default:
				throw new Exception($"Symbol {_symbol} is not supported.");
		}
	})
	// Functions
	.Simple(CalculatorToken.FUNCTION, (_symbol, _arguments) =>
	{
		int count = _arguments.Count();
		if (count != 1)
		{
			throw new Exception($"Function has {count} arguments.");
		}
		double argument = Double.Parse(_arguments[0].ToString());

		switch (_symbol.ToUpper())
		{
			case "SQRT":
				return Math.Sqrt(argument);
			case "SIN":
				return Math.Sin(argument);
			case "COS":
				return Math.Cos(argument);
			case "TAN":
				return Math.Tan(argument);
			case "COTAN":
				return 1 / Math.Tan(argument);
			default:
				throw new Exception($"Function {_symbol} is not supported.");
		}
	});
```

Create the parser:

```csharp
Parser parser = new Parser(lexer, syntaxer, interpretator);
```

Now you can use it:

```csharp
string result = parser.Parse("Sqrt((2 + 2) * 4)").ToString();
Console.WriteLine(result);
```

## Examples

Two example projects are provided.

### Simple

This example generates a bunch of files that illustrate the inner state of the parser between passes.
File contents look like this:

```
...
├┬ =                                                              ASSIGN (ComplexToken)
│├┬ 3ms                                                           NUMBER_IN_UNITS (SimpleToken)
││├─ 3                                                            NUMBER_REAL (SimpleToken)
││└─ ms                                                           UNITS (SimpleToken)
│└┬ +                                                             PLUS (ComplexToken)
│ ├┬ 1Hz                                                          NUMBER_IN_UNITS (SimpleToken)
│ │├─ 1                                                           NUMBER_REAL (SimpleToken)
│ │└─ Hz                                                          UNITS (SimpleToken)
│ └┬ ()                                                           BRACKET_ROUND (SimpleToken)
│  └┬ -                                                           MINUS (ComplexToken)
│   ├┬ *                                                          MULTIPLY (ComplexToken)
│   │├─ 2                                                         NUMBER_INTEGER (SimpleToken)
│   │└─ 5                                                         NUMBER_INTEGER (SimpleToken)
│   └─ 6                                                          NUMBER_INTEGER (SimpleToken)
...

```

### Calculator

A console calculator.

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.