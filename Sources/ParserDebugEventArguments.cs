﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class ParserDebugEventArguments : EventArgs
    {
        public ParserDebugEventArgumentStageType StageType { get; }
        public int PassIndex { get; }
        public string StageName { get; }

        public ParserDebugEventArguments(ParserDebugEventArgumentStageType passType)
            : this(passType, -1, null) { }
        public ParserDebugEventArguments(ParserDebugEventArgumentStageType passType, int passIndex, string passName)
        {
            StageType = passType;
            PassIndex = passIndex;
            StageName = passName;
        }
    }
}
