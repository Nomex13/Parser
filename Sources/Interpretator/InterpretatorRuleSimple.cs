﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class InterpretatorRuleSimple : InterpretatorRule
    {
        private int Count { get; }
        private Func<string, object[], object> Function { get; }
        public InterpretatorRuleSimple(Enum token, Func<string, object[], object> function)
        {
            Count = -1;
            Function = function;
            Match(token);
        }
        public InterpretatorRuleSimple(Enum token, int count, Func<string, object[], object> function)
        {
            Count = count;
            Function = function;
            Match(token);
        }
        protected override void CustomInit()
        {
            ;
        }
        protected override void CustomDeinit()
        {
            ;
        }
        public override bool Process(int token)
        {
            if (Count >= 0 && Count != TokenManager.GetDownCount(token))
            {
                return false;
            }

            string symbol = TokenManager.GetSymbol(token);
            object[] objects = TokenManager.GetObjects(token);
            object @object = Function.Invoke(symbol, objects);
            TokenManager.SetObject(token, @object);

            return true;
        }
    }
    public static partial class InterpretatorExtensions
    {
        public static Interpretator Simple(this Interpretator interpretator, Enum token, Func<string, object[], object> function)
        {
            interpretator.AddRule(new InterpretatorRuleSimple(token, function));
            return interpretator;
        }
        public static Interpretator Simple(this Interpretator interpretator, Enum token, int count, Func<string, object[], object> function)
        {
            interpretator.AddRule(new InterpretatorRuleSimple(token, count, function));
            return interpretator;
        }
    }
}
