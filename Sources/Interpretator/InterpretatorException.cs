﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class InterpretatorException : Exception
    {
        public readonly int Line;
        public readonly int Position;

        public InterpretatorException(string message)
            : this(message, null) { }
        public InterpretatorException(string message, Exception innerException)
            : base(message, innerException)
        {
            ;
        }
    }
}
