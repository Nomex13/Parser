﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Parsing
{
    public abstract class InterpretatorRule
    {
        private HashSet<Enum> MatchedEnums { get; } = new HashSet<Enum>();
        internal List<ulong> MatchedTypes { get; } = new List<ulong>();
        protected Parser Parser { get; private set; }
        protected Interpretator Interpretator { get; set; }
        protected TokenManager TokenManager { get; private set; }

        public InterpretatorRule()
        {
            ;
        }
        internal void Init(Interpretator interpretator)
        {
            // Set references
            Parser = interpretator.Parser;
            Interpretator = interpretator;
            TokenManager = interpretator.TokenManager;
            // Enums to ids
            foreach (Enum @enum in MatchedEnums)
            {
                MatchedTypes.Add(Parser.ConvertEnumToTokenId(@enum));
            }
            // Custom
            CustomInit();
        }
        protected virtual void CustomInit()
        {
            ;
        }
        internal void Deinit()
        {
            // Custom
            CustomDeinit();
            // Clear ids
            MatchedTypes.Clear();
            // Clear references
            TokenManager = null;
            Interpretator = null;
            Parser = null;
        }
        protected virtual void CustomDeinit()
        {
            TokenManager = null;
        }
        public virtual bool Process(int token)
        {
            throw new NotImplementedException();
        }
        protected void Match(Enum @enum)
        {
            MatchedEnums.Add(@enum);
        }
        protected void Match(IEnumerable<Enum> enums)
        {
            foreach (Enum @enum in enums)
            {
                MatchedEnums.Add(@enum);
            }
        }

        //public int ArgumentsCount = 0;
        //public virtual void Make(IEnumerable<object> arguments)
        //{
        //    throw new NotImplementedException();
        //}
        //public virtual void Make<T>(params T[] arguments)
        //{
        //    throw new NotImplementedException();
        //}
        //public virtual void Make<T1>(Action<T1> argument1)
        //{
        //    throw new NotImplementedException();
        //}
        //public virtual void Make<T1, T2>(T1 argument1, T2 argument2)
        //{
        //    throw new NotImplementedException();
        //}
        //public virtual void Make<T1, T2, T3>(T1 argument1, T2 argument2, T3 argument3)
        //{
        //    throw new NotImplementedException();
        //}
        //public virtual void Make<T1, T2, T3, T4>(T1 argument1, T2 argument2, T3 argument3, T4 argument4)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
