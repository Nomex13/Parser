﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Parsing
{
    public class Interpretator
    {
        internal bool Debug { get; private set; }
		private List<InterpretatorRule> Rules { get; } = new List<InterpretatorRule>();
        private Dictionary<ulong, List<InterpretatorRule>> RulesByType { get; } = new Dictionary<ulong, List<InterpretatorRule>>();
        public Parser Parser { get; private set; }
        public TokenManager TokenManager => Parser.TokenManager;

        public Interpretator()
        {
            ;
        }
        internal void Initialize(Parser parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException(nameof(parser));
            }

            Debug = parser.Debug;
            Parser = parser;
            //TokenManager = Parser.TokenManager;
            foreach (InterpretatorRule rule in Rules)
            {
                rule.Init(this);
                // Fill the cache
                foreach (ulong type in rule.MatchedTypes)
                {
                    if (!RulesByType.TryGetValue(type, out List<InterpretatorRule> rules))
                    {
                        rules = new List<InterpretatorRule>();
                        RulesByType.Add(type, rules);
                    }
                    rules.Add(rule);
                }
            }
        }
        internal void Deinitialize()
        {
            RulesByType.Clear();
            foreach (InterpretatorRule rule in Rules)
            {
                rule.Deinit();
            }
            //TokenManager = null;
            Parser = null;
            Debug = false;
        }
		internal void Process()
		{
            int token = TokenManager.GetDeeplyFirst(1 /* The first root */);
            try
            {
                while (token != 0)
                {
                    ulong type = TokenManager.GetType(token);
                    if (RulesByType.TryGetValue(type, out List<InterpretatorRule> rules))
                    {
                        foreach (InterpretatorRule rule in rules)
                        {
                            if (rule.Process(token))
                            {
                                // Only one rule of the pass can be applied at once
                                break;
                            }
                        }
                    }
                    token = TokenManager.GetNextReversed(token);
                }

                if (Debug)
                {
                    Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.INTERPRETER_PASS_AFTER_SUCCESS, 0, null));
                }
            }
            catch (Exception exception)
            {
                if (Debug)
                {
                    Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.INTERPRETER_PASS_AFTER_ERROR, 0, null));
                }

                string symbol = null;
                string text = null;
                int length = -1;
                int line = -1;
                int position = -1;
                try
                {
                    symbol = TokenManager.GetSymbol(token);
                    text = TokenManager.GetText(token);
                    length = TokenManager.GetTextEnd(token) - TokenManager.GetTextStart(token) + 1;
                    (line, position) = TokenManager.GetLineAndPosition(token);
                }
                catch
                {
                    throw new Exception("Failed to interpret.", exception);
                }
                throw new InterpretatorException($"Failed to interpret {symbol?.Trim()} at line {line}, position from {position} to {position + length - 1}: {text?.Trim()}.", exception);
            }
		}
        public Interpretator AddRule(InterpretatorRule rule)
        {
            Rules.Add(rule);
            return this;
        }
    }
}
