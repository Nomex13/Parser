﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class LexerException : Exception
    {
        public readonly int Line;
        public readonly int Position;

        public LexerException(int line, int position)
            : this(line, position, null, null) { }
        public LexerException(int line, int position, string message)
            : this(line, position, message, null) { }
        public LexerException(int line, int position, Exception innerException)
            : this(line, position, null, innerException) { }
        public LexerException(int line, int position, string message, Exception innerException)
            : base(message, innerException)
        {
            Line = line;
            Position = position;
        }
    }
}
