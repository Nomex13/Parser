﻿using System;
using System.Text;

namespace Iodynis.Libraries.Parsing
{
    public class LexerMatcherStringBlock : LexerMatcher
    {
        private string OpenSequence;
        private bool IncludeOpenSequence;
        private string CloseSequence;
        private bool IncludeCloseSequence;
        private string CloseSequenceEscaped;

        StringBuilder stringBuilder = new StringBuilder();

        public LexerMatcherStringBlock(Enum token, string sequence, bool include)
            : this(token, sequence, include, sequence, include, null) { }
        public LexerMatcherStringBlock(Enum token, string sequence, bool include, string sequenceEscaped)
            : this(token, sequence, include, sequence, include, sequenceEscaped) { }
        public LexerMatcherStringBlock(Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence)
            : this(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence, null) { }
        // TODO: Escaped sequence will faile at "wtf\\" string. Need to provide escaped sequences to skip, like \\ in this case.
        public LexerMatcherStringBlock(Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, string closeSequenceEscaped)
            : base(token)
        {
            if (openSequence == null)
            {
                throw new ArgumentNullException(nameof(openSequence));
            }
            if (closeSequence == null)
            {
                throw new ArgumentNullException(nameof(closeSequence));
            }

            OpenSequence = openSequence;
            IncludeOpenSequence = includeOpenSequence;
            CloseSequence = closeSequence;
            IncludeCloseSequence = includeCloseSequence;
            CloseSequenceEscaped = closeSequenceEscaped;

            // Escaped sequence with zero length is not used
            if (CloseSequenceEscaped != null && CloseSequenceEscaped.Length == 0)
            {
                CloseSequenceEscaped = null;
            }
        }
        public override LexerMatch Match(string text, int index)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            // Maximum possible match length for the given text
            int matchLengthMaximum = text.Length - index;
            // Check if open and close sequence can actually fit in
            if (OpenSequence.Length + CloseSequence.Length > matchLengthMaximum)
            {
                return null; // Not enough character are left
            }

            int matchIndex = 0;
            // Check if the opening sequence matches
            for (; matchIndex < OpenSequence.Length; matchIndex++)
            {
                // Check if differencies found
                if (OpenSequence[matchIndex] != text[index + matchIndex])
                {
                    return null;
                }
            }

            int matchLengthMaximumWithoutClosingSequence = matchLengthMaximum - CloseSequence.Length;

            // If the escape sequence is defined some replacements may occur, so need to collect the match character-wise
            if (CloseSequenceEscaped != null)// && matchLength < matchLengthMaximumWithoutClosingSequenceEscaped)
            {
                stringBuilder.Clear();

                int matchLengthMaximumWithoutClosingSequenceEscaped = matchLengthMaximum - CloseSequenceEscaped.Length;

                if (IncludeOpenSequence)
                {
                    stringBuilder.Append(OpenSequence);
                }
                // Move through the text character-wise
                for (; matchIndex <= matchLengthMaximumWithoutClosingSequence; matchIndex++)
                {
                    // If the escaped sequence may fit in
                    if (matchIndex < matchLengthMaximumWithoutClosingSequenceEscaped)
                    {
                        // Check if the closing escaped sequence matches
                        for (int sequenceIndex = 0; sequenceIndex <= CloseSequenceEscaped.Length; sequenceIndex++)
                        {
                            // Check if the closing escaped sequence is fully matched
                            if (sequenceIndex == CloseSequenceEscaped.Length)
                            {
                                // Collect the true closing sequence instead of the escaped one
                                stringBuilder.Append(CloseSequence);

                                // Shift the main index
                                matchIndex += sequenceIndex - 1 /* because of the autoincrement of the for-loop */;

                                // Go straight to the next character analysis
                                goto next;
                            }
                            // Check if differencies found
                            else if (CloseSequenceEscaped[sequenceIndex] != text[index + matchIndex + sequenceIndex])
                            {
                                break;
                            }
                        }
                    }
                    // Check if the closing sequence matches
                    for (int closeIndex = 0; closeIndex <= CloseSequence.Length; closeIndex++)
                    {
                        // Check if the closing sequence is fully matched
                        if (closeIndex == CloseSequence.Length)
                        {
                            // Finalize collecting the match
                            if (IncludeCloseSequence)
                            {
                                stringBuilder.Append(CloseSequence);
                            }
                            return new LexerMatch(TokenId, stringBuilder.ToString(), index, index + matchIndex + CloseSequence.Length);
                        }
                        // Check if differencies found
                        else if (CloseSequence[closeIndex] != text[index + matchIndex + closeIndex])
                        {
                            break;
                        }
                    }
                    // Collect the new character
                    stringBuilder.Append(text[index + matchIndex]);

                    next:;
                }
                // Ooops, the closing sequence was not found
                throw new Exception($"String block matcher failed to find closing sequence {CloseSequence} after opening sequence {OpenSequence}.");
            }
            // If no escape sequence is defined then only the ending sequence should be found.
            // A simple substring may be used then.
            else
            {
                // Move through the text character-wise
                for (; matchIndex <= matchLengthMaximumWithoutClosingSequence; matchIndex++)
                {
                    // Check if the closing sequence matches
                    for (int closeIndex = 0; closeIndex <= CloseSequence.Length; closeIndex++)
                    {
                        // Check if the closing sequence is fully matched
                        if (closeIndex == CloseSequence.Length)
                        {
                            string @string = text.Substring(index + (IncludeOpenSequence ? 0 : OpenSequence.Length), matchIndex - (IncludeOpenSequence ? 0 : OpenSequence.Length) + (IncludeCloseSequence ? CloseSequence.Length : 0));
                            return new LexerMatch(TokenId, @string, index, index + matchIndex + CloseSequence.Length);
                        }
                        // Check if differencies found
                        if (CloseSequence[closeIndex] != text[index + matchIndex + closeIndex])
                        {
                            break;
                        }
                    }
                }
            }
            return null;
        }
    }
    public static partial class LexerExtensions
    {
        public static Lexer StringBlock(this Lexer lexer, Enum token, string sequence, bool include)
        {
            lexer.AddMatcher(new LexerMatcherStringBlock(token, sequence, include));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string sequence, bool include, string sequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherStringBlock(token, sequence, include, sequenceEscaped));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence)
        {
            lexer.AddMatcher(new LexerMatcherStringBlock(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, string closeSequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherStringBlock(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence, closeSequenceEscaped));
            return lexer;
        }
    }
}
