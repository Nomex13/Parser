﻿using System;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Lexer mode defines how to resolve multiple matches.
    /// </summary>
    public enum LexerMode
    {
        /// <summary>
        /// Use the very first successful match.
        /// </summary>
        MATCH_FIRST,
        /// <summary>
        /// In case of multiple tokens matched use the shortest one.
        /// </summary>
        MATCH_SHORTEST,
        /// <summary>
        /// In case of multiple tokens matched use the longest one. This is the default behavior for a lexer.
        /// </summary>
        MATCH_LONGEST,
    }
    public static partial class Extensions
    {
        public static LexerPassMode ToLexerPassMode(this LexerMode mode)
        {
            switch (mode)
            {
                case LexerMode.MATCH_FIRST:
                    return LexerPassMode.MATCH_FIRST;
                case LexerMode.MATCH_LONGEST:
                    return LexerPassMode.MATCH_LONGEST;
                case LexerMode.MATCH_SHORTEST:
                    return LexerPassMode.MATCH_SHORTEST;
                default:
                    throw new InvalidCastException($"Lexer mode of {mode} cannot be cast to lexer pass mode.");
            }
        }
    }
}
