﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class LexerMatcherStringBlockEscaped : LexerMatcher
    {
        private string OpenSequence;
        private bool IncludeOpenSequence;
        private string CloseSequence;
        private bool IncludeCloseSequence;
        private Dictionary<string, string> EscapeSequences;

        StringBuilder StringBuilder { get; } = new StringBuilder();

        public LexerMatcherStringBlockEscaped(Enum token, string sequence, bool include)
            : this(token, sequence, include, sequence, include, null) { }
        public LexerMatcherStringBlockEscaped(Enum token, string sequence, bool include, Dictionary<string, string> escapeSequences)
            : this(token, sequence, include, sequence, include, escapeSequences) { }
        public LexerMatcherStringBlockEscaped(Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence)
            : this(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence, null) { }
        public LexerMatcherStringBlockEscaped(Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, Dictionary<string, string> escapeSequences)
            : base(token)
        {
            if (openSequence == null)
            {
                throw new ArgumentNullException(nameof(openSequence));
            }
            if (closeSequence == null)
            {
                throw new ArgumentNullException(nameof(closeSequence));
            }

            OpenSequence = openSequence;
            IncludeOpenSequence = includeOpenSequence;
            CloseSequence = closeSequence;
            IncludeCloseSequence = includeCloseSequence;
            EscapeSequences = escapeSequences == null ? new Dictionary<string, string>() : new Dictionary<string, string>(escapeSequences);
        }
        public override LexerMatch Match(string text, int index)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            // Maximum possible match length for the given text
            int matchLengthMaximum = text.Length - index;
            // Check if open and close sequence can actually fit in
            if (OpenSequence.Length + CloseSequence.Length > matchLengthMaximum)
            {
                return null; // Not enough character are left
            }

            int matchIndex = 0;
            // Check if the opening sequence matches
            for (; matchIndex < OpenSequence.Length; matchIndex++)
            {
                // Check if differencies found
                if (OpenSequence[matchIndex] != text[index + matchIndex])
                {
                    return null;
                }
            }

            int matchLengthMaximumWithoutClosingSequence = matchLengthMaximum - CloseSequence.Length;
            //int matchLengthMaximumWithoutClosingSequenceEscaped = matchLengthMaximum - CloseSequenceEscaped.Length;

            StringBuilder.Clear();
            if (IncludeOpenSequence)
            {
                StringBuilder.Append(OpenSequence);
            }
            // Move through the text character-wise
            for (; matchIndex <= matchLengthMaximumWithoutClosingSequence; matchIndex++)
            {
                foreach (string escapeSequence in EscapeSequences.Keys)
                {
                    // If the escaped sequence may fit in
                    if (matchIndex <  matchLengthMaximum - escapeSequence.Length)
                    {
                        // Check if the escaped sequence matches
                        for (int escapeIndex = 0; escapeIndex <= escapeSequence.Length; escapeIndex++)
                        {
                            // Check if the escaped sequence is fully matched
                            if (escapeIndex == escapeSequence.Length)
                            {
                                // Collect the true sequence instead of the escaped one
                                StringBuilder.Append(EscapeSequences[escapeSequence]);

                                // Shift the main index
                                matchIndex += escapeIndex - 1 /* because of the autoincrement of the for-loop */;

                                // Go straight to the next character analysis
                                goto next;
                            }
                            // Check if differencies found
                            else if (escapeSequence[escapeIndex] != text[index + matchIndex + escapeIndex])
                            {
                                break;
                            }
                        }
                    }
                }

                //// Move through the text character-wise
                // Check if the closing sequence matches
                for (int closeIndex = 0; closeIndex <= CloseSequence.Length; closeIndex++)
                {
                    // Check if the closing sequence is fully matched
                    if (closeIndex == CloseSequence.Length)
                    {
                        // Finalize collecting the match
                        if (IncludeCloseSequence)
                        {
                            StringBuilder.Append(CloseSequence);
                        }
                        return new LexerMatch(TokenId, StringBuilder.ToString(), index, index + matchIndex + CloseSequence.Length);
                    }
                    // Check if differencies found
                    else if (CloseSequence[closeIndex] != text[index + matchIndex + closeIndex])
                    {
                        break;
                    }
                }

                // Collect the new character
                StringBuilder.Append(text[index + matchIndex]);

                next:;
            }
            // Ooops, the closing sequence was not found
            //stringBuilder.Append(text.Substring(index + matchIndex));
            throw new Exception($"String block escaped matcher failed to find closing sequence {CloseSequence} after opening sequence {OpenSequence}.");
        }
    }
    public static partial class LexerExtensions
    {
        public static Lexer StringBlockEscaped(this Lexer lexer, Enum token, string sequence, bool include)
        {
            lexer.AddMatcher(new LexerMatcherStringBlockEscaped(token, sequence, include));
            return lexer;
        }
        public static Lexer StringBlockEscaped(this Lexer lexer, Enum token, string sequence, bool include, Dictionary<string, string> escapeSequences)
        {
            lexer.AddMatcher(new LexerMatcherStringBlockEscaped(token, sequence, include, escapeSequences));
            return lexer;
        }
        public static Lexer StringBlockEscaped(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence)
        {
            lexer.AddMatcher(new LexerMatcherStringBlockEscaped(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence));
            return lexer;
        }
        public static Lexer StringBlockEscaped(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, Dictionary<string, string> escapeSequences)
        {
            lexer.AddMatcher(new LexerMatcherStringBlockEscaped(token, openSequence, includeOpenSequence, closeSequence, includeCloseSequence, escapeSequences));
            return lexer;
        }
    }
}
