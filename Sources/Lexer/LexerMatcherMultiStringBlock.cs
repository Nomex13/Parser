﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Iodynis.Libraries.Parsing
{
    public class LexerMatcherMultiStringBlock : LexerMatcher
    {
        private List<string> OpenSequences = new List<string>();
        private bool IncludeOpenSequence;
        private List<string> CloseSequences = new List<string>();
        private bool IncludeCloseSequence;
        private List<string> CloseSequencesEscaped = new List<string>();
        private bool MultipleEscapeSequenceMode = false;
        private bool SingularEscapeSequenceMode = false;

        StringBuilder stringBuilder = new StringBuilder();

        public LexerMatcherMultiStringBlock(Enum token, IEnumerable<string> sequences, bool include)
            : this(token, sequences, include, sequences, include, null) { }
        public LexerMatcherMultiStringBlock(Enum token, IEnumerable<string> sequences, bool include, IEnumerable<string> sequencesEscaped)
            : this(token, sequences, include, sequences, include, sequencesEscaped) { }
        public LexerMatcherMultiStringBlock(Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence)
            : this(token, openSequences, includeOpenSequence, closeSequences, includeCloseSequence, new string[0]) { }
        public LexerMatcherMultiStringBlock(Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence, IEnumerable<string> closeSequencesEscaped)
            : base(token)
        {
            if (openSequences == null)
            {
                throw new ArgumentNullException(nameof(openSequences));
            }
            if (closeSequences == null)
            {
                throw new ArgumentNullException(nameof(closeSequences));
            }
            if (closeSequencesEscaped == null)
            {
                throw new ArgumentNullException(nameof(closeSequencesEscaped));
            }
            foreach (string closeSequenceEscaped in closeSequencesEscaped)
            {
                if (closeSequenceEscaped == null || closeSequenceEscaped.Length == 0)
                {
                    throw new ArgumentException("Escaped close sequence cannot be null or empty.", nameof(closeSequencesEscaped));
                }
            }

            OpenSequences.AddRange(openSequences);
            IncludeOpenSequence = includeOpenSequence;
            CloseSequences.AddRange(closeSequences);
            IncludeCloseSequence = includeCloseSequence;
            CloseSequencesEscaped.AddRange(closeSequencesEscaped);

            if (CloseSequencesEscaped.Count == 0)
            {
                ;
            }
            else if (CloseSequencesEscaped.Count == 1)
            {
                SingularEscapeSequenceMode = true;
            }
            else if (CloseSequencesEscaped.Count == CloseSequences.Count)
            {
                MultipleEscapeSequenceMode = true;
            }
            else
            {
                throw new ArgumentException("Escaped close sequence count should be 0, 1, or equal to close sequences count.", nameof(closeSequencesEscaped));
            }
        }
        public override LexerMatch Match(string text, int index)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            throw new NotImplementedException();
            //// Maximum possible match length for the given text
            //int matchLengthMaximum = text.Length - index;
            //// Check if open and close sequence can actually fit in
            //if (OpenSequences.Length + CloseSequences.Length > matchLengthMaximum)
            //{
            //    return null; // Not enough character are left
            //}

            //int matchIndex = 0;
            //// Check if the opening sequence matches
            //for (; matchIndex < OpenSequences.Length; matchIndex++)
            //{
            //    // Check if differencies found
            //    if (OpenSequences[matchIndex] != text[index + matchIndex])
            //    {
            //        return null;
            //    }
            //}

            //int matchLengthMaximumWithoutClosingSequence = matchLengthMaximum - CloseSequences.Length;

            //// If the escape sequence is defined some replacements may occur, so need to collect the match character-wise
            //if (CloseSequenceEscaped != null)// && matchLength < matchLengthMaximumWithoutClosingSequenceEscaped)
            //{
            //    stringBuilder.Clear();

            //    int matchLengthMaximumWithoutClosingSequenceEscaped = matchLengthMaximum - CloseSequenceEscaped.Length;

            //    if (IncludeOpenSequence)
            //    {
            //        stringBuilder.Append(OpenSequences);
            //    }
            //    // Move through the text character-wise
            //    for (; matchIndex <= matchLengthMaximumWithoutClosingSequence; matchIndex++)
            //    {
            //        // If the escaped sequence may fit in
            //        if (matchIndex < matchLengthMaximumWithoutClosingSequenceEscaped)
            //        {
            //            // Check if the closing escaped sequence matches
            //            for (int sequenceIndex = 0; sequenceIndex <= CloseSequenceEscaped.Length; sequenceIndex++)
            //            {
            //                // Check if the closing escaped sequence is fully matched
            //                if (sequenceIndex == CloseSequenceEscaped.Length)
            //                {
            //                    // Collect the true closing sequence instead of the escaped one
            //                    stringBuilder.Append(CloseSequences);

            //                    // Shift the main index
            //                    matchIndex += sequenceIndex - 1 /* because of the autoincrement of the for-loop */;

            //                    // Go straight to the next character analysis
            //                    goto next;
            //                }
            //                // Check if differencies found
            //                else if (CloseSequenceEscaped[sequenceIndex] != text[index + matchIndex + sequenceIndex])
            //                {
            //                    break;
            //                }
            //            }
            //        }
            //        // Check if the closing sequence matches
            //        for (int closeIndex = 0; closeIndex <= CloseSequences.Length; closeIndex++)
            //        {
            //            // Check if the closing sequence is fully matched
            //            if (closeIndex == CloseSequences.Length)
            //            {
            //                // Finalize collecting the match
            //                if (IncludeCloseSequence)
            //                {
            //                    stringBuilder.Append(CloseSequences);
            //                }
            //                return new LexerMatch(this, stringBuilder.ToString(), matchIndex + CloseSequences.Length);
            //            }
            //            // Check if differencies found
            //            else if (CloseSequences[closeIndex] != text[index + matchIndex + closeIndex])
            //            {
            //                break;
            //            }
            //        }
            //        // Collect the new character
            //        stringBuilder.Append(text[index + matchIndex]);

            //        next:;
            //    }
            //    // Ooops, the closing sequence was not found
            //    //stringBuilder.Append(text.Substring(index + matchIndex));
            //    throw new Exception("Closing sequence was not found.");
            //}
            //// If no escape sequence is defined then only the ending sequence should be found.
            //// A simple substring may be used then.
            //else
            //{
            //    // Move through the text character-wise
            //    for (; matchIndex <= matchLengthMaximumWithoutClosingSequence; matchIndex++)
            //    {
            //        // Check if the closing sequence matches
            //        for (int closeIndex = 0; closeIndex <= CloseSequences.Length; closeIndex++)
            //        {
            //            // Check if the closing sequence is fully matched
            //            if (closeIndex == CloseSequences.Length)
            //            {
            //                string @string = text.Substring(index + (IncludeOpenSequence ? 0 : OpenSequences.Length), matchIndex + 1 - (IncludeOpenSequence ? 0 : OpenSequences.Length) - (IncludeCloseSequence ? 0 : CloseSequences.Length));
            //                return new LexerMatch(this, @string, matchIndex);
            //            }
            //            // Check if differencies found
            //            if (CloseSequences[closeIndex] != text[index + matchIndex + closeIndex])
            //            {
            //                break;
            //            }
            //        }
            //    }
            //}
            //return null;
        }
    }
    public static partial class LexerExtensions
    {
        public static Lexer MatchStringBlock(this Lexer lexer, Enum token, IEnumerable<string> sequences, bool include)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, sequences, include));
            return lexer;
        }
        public static Lexer MatchStringBlock(this Lexer lexer, Enum token, string sequence, bool include, IEnumerable<string> sequencesEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, new string[] { sequence }, include, sequencesEscaped));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> sequences, bool include, string sequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, sequences, include, new string[] { sequenceEscaped }));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> sequences, bool include, IEnumerable<string> sequencesEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, sequences, include, sequencesEscaped));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, new string[] { openSequence }, includeOpenSequence, closeSequences, includeCloseSequence));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, string closeSequence, bool includeCloseSequence)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, new string[] { closeSequence }, includeCloseSequence));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, closeSequences, includeCloseSequence));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence, string closeSequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, new string[] { openSequence }, includeOpenSequence, closeSequences, includeCloseSequence, new string [] { closeSequenceEscaped }));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, string openSequence, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence, IEnumerable<string> closeSequencesEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, new string[] { openSequence }, includeOpenSequence, closeSequences, includeCloseSequence, closeSequencesEscaped));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, string closeSequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, new string[] { closeSequence }, includeCloseSequence, new string[] { closeSequenceEscaped }));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, string closeSequence, bool includeCloseSequence, IEnumerable<string> closeSequencesEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, new string[] { closeSequence }, includeCloseSequence, closeSequencesEscaped));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence, string closeSequenceEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, closeSequences, includeCloseSequence, new string[] { closeSequenceEscaped }));
            return lexer;
        }
        public static Lexer StringBlock(this Lexer lexer, Enum token, IEnumerable<string> openSequences, bool includeOpenSequence, IEnumerable<string> closeSequences, bool includeCloseSequence, IEnumerable<string> closeSequencesEscaped)
        {
            lexer.AddMatcher(new LexerMatcherMultiStringBlock(token, openSequences, includeOpenSequence, closeSequences, includeCloseSequence, closeSequencesEscaped));
            return lexer;
        }
    }
}
