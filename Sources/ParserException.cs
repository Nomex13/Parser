﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class ParserException : Exception
    {
        public readonly int Line;
        public readonly int Position;

        //public ParserException(int line, int position, string message, Exception exception)
        //    : base(message, exception)
        //{
        //    Line = line;
        //    Position = position;
        //}

        public ParserException(LexerException innerException)
            : base("Failed to parse.", innerException)
        {
            Line = innerException.Line;
            Position = innerException.Position;
        }
        public ParserException(string message, LexerException innerException)
            : base(message, innerException)
        {
            Line = innerException.Line;
            Position = innerException.Position;
        }

        public ParserException(SyntaxerException innerException)
            : base("", innerException)
        {
            Line = innerException.Line;
            Position = innerException.Position;
        }
        public ParserException(string message, SyntaxerException innerException)
            : base(message, innerException)
        {
            Line = innerException.Line;
            Position = innerException.Position;
        }
    }
}
