﻿namespace Iodynis.Libraries.Parsing
{
    public enum TokenBasic : int
    {
        ZERO  = 0,
        ROOT  = 1,
        TOKEN = 2,
    }
}
