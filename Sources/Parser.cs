﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Parsing
{
	public class Parser
	{
        private int EnumerationsMaximumCount = 65536 /* = 2^16 */;
        private int _EnumerationsReservedBitsCount = 16;
        /// <summary>
        /// Enumerations are internally stored in the parser as 64-bit integers.
        /// If enumerations used happen to be 32-bit or less it is possible to use multiple enumeration simultaneously.
        /// The information about the enumerations is stored in the higher bits of the internal 64-bit integer.
        /// Possible count of bits reserved is 0 .. 16.
        /// P.S. This will still work with 64-bit enumerations, but you must pay attention so that the reserved bits are not used by any of the enumerations.
        /// </summary>
        public int EnumerationsReservedBitsCount
        {
            get
            {
                return _EnumerationsReservedBitsCount;
            }
            set
            {
                if (_EnumerationsReservedBitsCount < 0)
                {
                    throw new Exception($"Enumerations reserved bits count cannot be negative. Possible values are 0 .. 16.");
                }
                else if (_EnumerationsReservedBitsCount == 0)
                {
                }
                else if (_EnumerationsReservedBitsCount > 16)
                {
                    throw new Exception($"Enumerations reserved bits count cannot be greater than 16. Possible values are 0 .. 16.");
                }
                _EnumerationsReservedBitsCount = value;
                EnumerationsMaximumCount = 1 << (_EnumerationsReservedBitsCount - 1);
            }
        }

        private Dictionary<Enum, ulong> TokenTypes = new Dictionary<Enum, ulong>();
        private List<Type> Enums = new List<Type>();
        private Type EnumerationOnlyType { get; set; } = null;
        private List<int> NewLinePositions = new List<int>();

        public bool Debug
        {
            get
            {
                return DebugEvent != null;
            }
        }
        public event EventHandler<ParserDebugEventArguments> DebugEvent;
        internal void OnDebugEvent(ParserDebugEventArguments arguments)
        {
            DebugEvent?.Invoke(this, arguments);
        }

        public TokenManager TokenManager { get; private set; }

        /// <summary>
        /// Lexer transforms a text string into a sequence of tokens.
        /// </summary>
		public Lexer Lexer { get; }

        /// <summary>
        /// Syntaxer transforms the sequence of tokens into a tree.
        /// </summary>
		public Syntaxer Syntaxer { get; }

        /// <summary>
        /// Emitter transforms tokens into expressions.
        /// </summary>
		public Interpretator Interpretator { get; }

		public Parser(Lexer lexer, Syntaxer syntaxer, Interpretator interpretator)
		{
            Lexer = lexer;
			Syntaxer = syntaxer;
            Interpretator = interpretator;
		}
        private bool isInited;
		public Parser()
		{
			Lexer = new Lexer();
            Syntaxer = new Syntaxer();
            Interpretator = new Interpretator();
        }

        public void Init()
        {
            if (isInited) return;

            Lexer.Initialize(this);
            Syntaxer.Initialize(this);
            Interpretator.Initialize(this);

            isInited = true;
        }
        public void Deinit()
        {
            if (!isInited) return;

            Interpretator.Deinitialize();
            Syntaxer.Deinitialize();
            Lexer.Deinitialize();

            isInited = false;
        }
        public object Parse(string text)
		{
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text), "Text is null.");
            }
            if (text == "")
            {
                return new object[0];
            }
            object[] objects = Parse(new string[] { text });
            if (objects.Length == 0)
            {
                return null;
            }
            return objects[0];
        }
		public object[] Parse(IEnumerable<string> texts)
        {
            if (texts.Any(_text => _text == null))
            {
                throw new ArgumentNullException(nameof(texts), "One of the texts is null.");
            }
            #if !DEBUG
            try
            {
            #endif
                int capacityExpected = texts.Sum(_text => _text.Length) / 2;
                //if (TokenManager == null)
                //{
                    TokenManager = new TokenManager(capacityExpected);// { Mode = TokenManager.TokenManagerMode.RECYCLE_RESET };
                //}
                //else
                //{
                //    TokenManager.Reset(capacityExpected);
                //}

                foreach (string text in texts)
                {
                    TokenManager.AddRoot(text);
                }

                // Ensure that everything is initialized
                bool isInitedAutomatically = !isInited;
                if (isInitedAutomatically)
                {
                    Init();
                }

                // Break text into tokens
                try
                {
                    Lexer.Process();
                }
                catch
                {
                    OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.LEXER_PASS_AFTER_SUCCESS));
                    throw;
                }
                // Manipulate tokens
                try
                {
                    Syntaxer.Process();
                }
                catch
                {
                    OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.SYNTAXER_PASS_AFTER_SUCCESS));
                    throw;
                }
                // Make it executable
                try
                {
                    int token = TokenManager.CheckConsistency();
                    if (token != 0)
                    {
                        throw new Exception($"Inner consistency chack failed on token {token}.");
                    }
                }
                catch
                {
                    OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.INTERPRETER_PASS_AFTER_SUCCESS));
                    throw;
                }
                // Make it executable
                try
                {
                    Interpretator.Process();
                }
                catch
                {
                    OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.INTERPRETER_PASS_AFTER_SUCCESS));
                    throw;
                }

                // If initialization was automatic -- deinitialize to revert to the original state
                if (isInitedAutomatically)
                {
                    Deinit();
                }

                File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "debug.txt"), TokenManager.PrintSimple(this, 128), Encoding.UTF8);

                // TODO: Fix the hack
                return TokenManager.GetObjects(1);
            #if !DEBUG
            }
            catch (LexerException exception)
            {
                throw new ParserException($"Failed to parse. Lexeical analysis failed.", exception);
            }
            catch (SyntaxerException exception)
            {
                throw new ParserException($"Failed to parse. Syntax analysis failed.", exception);
            }
            catch (InterpretatorException exception)
            {
                throw new Exception($"Failed to parse. Interpretation failed.", exception);
            }
            catch (Exception exception)
            {
                throw new Exception($"Failed to parse.", exception);
            }
            #endif
        }

        #region Util
        public Enum ConvertTokenIdToEnum(ulong token)
        {
            if (token < 0)
            {
                return null;
            }
            int enumIndex = (int)(token >> (64 - EnumerationsReservedBitsCount));
            int enumValue = (int)token;
            return (Enum)Enum.ToObject(Enums[enumIndex], enumValue);
        }
        public ulong ConvertEnumToTokenId(Enum token)
        {
            if (token == null)
            {
                return 0;
            }
            if (EnumerationsReservedBitsCount > 0)
            {
                Type tokenType = token.GetType();
                if (!TokenTypes.TryGetValue(token, out ulong type))
                {
                    if (TokenTypes.Count >= EnumerationsMaximumCount)
                    {
                        throw new Exception($"Maximum enumerations count of {EnumerationsMaximumCount} is exceeded. Please consider reserving more bits for the enumerations.");
                    }
                    type = ((ulong)TokenTypes.Count) << (64 - EnumerationsReservedBitsCount);
                    Enums.Add(tokenType);
                    TokenTypes.Add(token, type);
                }
                return ConvertEnumToLong(token) + type;
            }
            else
            {
                Type tokenType = token.GetType();
                if (EnumerationOnlyType == null)
                {
                    EnumerationOnlyType = tokenType;
                }
                else if (EnumerationOnlyType != tokenType)
                {
                    throw new Exception($"Token types mismatch. Token type is already set to {EnumerationOnlyType} while a new type {tokenType} is provided. To use different token types consider setting EnumerationsReservedBitsCount property.");
                }
                return ConvertEnumToLong(token);
            }
        }
        private ulong ConvertEnumToLong(Enum @enum)
        {
            Type enumType = @enum.GetType();
            Type enumUnderlyingType = Enum.GetUnderlyingType(enumType);
            if (enumUnderlyingType == typeof(int))
            {
                return (ulong)(int)(object)@enum;
            }
            else if (enumUnderlyingType == typeof(short))
            {
                return (ulong)(short)(object)@enum;
            }
            else if (enumUnderlyingType == typeof(byte))
            {
                return (ulong)(byte)(object)@enum;
            }
            else if (enumUnderlyingType == typeof(long))
            {
                return (ulong)(object)@enum;
            }
            else
            {
                throw new Exception($"The enumration underlying type of {enumUnderlyingType} is not supported. Please use byte, short or int. You may use long as well if you do not need to use different enumerations simultaneously (technically you still can but cautiously, see documentation for details).");
            }
        }
        //private static List<int> FindNewLines(string text)
        //{
        //    List<int> newLines = new List<int>();

        //    for (int index = 0; index < text.Length; index++)
        //    {
        //        if (text[index] == '\n')
        //        {
        //            newLines.Add(index);
        //        }
        //    }

        //    return newLines;
        //}
        #endregion
	}
}
