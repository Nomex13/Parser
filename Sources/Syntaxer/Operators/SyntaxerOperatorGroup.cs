﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Iodynis.Libraries.Parsing
//{
//	public class SyntaxerOperatorGroup : SyntaxerOperator
//	{
//		private int typeGroup;
//		private string symbolGroup;
//		private int typeOpen;
//		private bool includeOpen;
//		private int typeClose;
//		private bool includeClose;

//		public SyntaxerOperatorGroup(Enum typeGroup, Enum typeOpen, bool includeOpen, Enum typeClose, bool includeClose)
//			: this(typeGroup, null, typeOpen, includeOpen, typeClose, includeClose)
//		{
//            ;
//        }
//		public SyntaxerOperatorGroup(Enum typeGroup, string symbolGroup, Enum typeOpen, bool includeOpen, Enum typeClose, bool includeClose)
//			: base()
//		{
//			this.typeGroup = (int)(object)typeGroup;
//			this.symbolGroup = symbolGroup;
//            this.typeOpen  = (int)(object)typeOpen;
//            this.includeOpen  = includeOpen;
//            this.typeClose = (int)(object)typeClose;
//            this.includeClose  = includeClose;
//		}

//		public override void Operate(ref int token)
//		{
//            int tokenCurrent = token;
//            int depth = 0;
//            while (tokenCurrent != 0)
//            {
//                int type = TokenManager.GetType(tokenCurrent);
//                if (type == typeOpen)
//                {
//                    depth++;
//                }
//                else if (type == typeClose)
//                {
//                    depth--;
//                }
//                if (depth == 0)
//                {
//                    if (symbolGroup != null)
//                    {
//                        token = TokenManager.Group(typeGroup, symbolGroup, token, includeOpen, tokenCurrent, includeClose);
//                    }
//                    else
//                    {
//                        token = TokenManager.Group(typeGroup, token, includeOpen, tokenCurrent, includeClose);
//                    }
//                    return;
//                }
//                tokenCurrent = TokenManager.GetRight(tokenCurrent);
//            }
//		}
//	}
//}
