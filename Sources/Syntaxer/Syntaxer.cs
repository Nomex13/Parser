﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Syntaxer transforms a list of tokens into a tree of tokens.
    /// </summary>
    /// <typeparam name="T">Enumeration of token types to use.</typeparam>
	public class Syntaxer
	{
        internal bool Debug { get; private set; }
		internal List<SyntaxerPass> Passes { get; } = new List<SyntaxerPass>();
        internal Parser Parser { get; private set; }
        public TokenManager TokenManager => Parser.TokenManager;

		public Syntaxer()
		{
            ;
		}
		public Syntaxer(IEnumerable<SyntaxerPass> passes)
		{
            if (passes == null)
            {
                throw new ArgumentNullException(nameof(passes));
            }

            Passes.AddRange(passes);
		}
        internal void Initialize(Parser parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException(nameof(parser));
            }

            Debug = parser.Debug;
            Parser = parser;
            //TokenManager = parser.TokenManager;
            foreach (SyntaxerPass pass in Passes)
            {
                pass.Initialize(this);
            }
        }
        internal void Deinitialize()
        {
            foreach (SyntaxerPass pass in Passes)
            {
                pass.Deinitialize();
            }
            //TokenManager = null;
            Parser = null;
            Debug = false;
        }
		internal void Process()
		{
            for (int passIndex = 0; passIndex < Passes.Count; passIndex++)
            {
                SyntaxerPass pass = Passes[passIndex];

                try
                {
                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.SYNTAXER_PASS_BEFORE, passIndex, pass.Name));
                    }

				    pass.Process();

                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.SYNTAXER_PASS_AFTER_SUCCESS, passIndex, pass.Name));
                    }
                }
                catch
                {
                    if (Debug)
                    {
                        Parser.OnDebugEvent(new ParserDebugEventArguments(ParserDebugEventArgumentStageType.SYNTAXER_PASS_AFTER_ERROR, passIndex, pass.Name));
                    }
                    throw;
                }
            }
		}
        public Syntaxer AddPass()
        {
            return AddPass(new SyntaxerPass());
        }
        public Syntaxer AddPass(string name)
        {
            return AddPass(new SyntaxerPass(name));
        }
        public Syntaxer AddPass(SyntaxerPass pass)
        {
            Passes.Add(pass);
            return this;
        }
        public Syntaxer AddRule(SyntaxerRule rule)
        {
            if (Passes.Count == 0)
            {
                AddPass(new SyntaxerPass($"#{Passes.Count + 1}"));
            }
            Passes.Last().AddRule(rule);
            return this;
        }
	}
}
