﻿using System;

namespace Iodynis.Libraries.Parsing
{
	public class SyntaxerCheckerForType : SyntaxerChecker
	{
		private ulong type;

		public SyntaxerCheckerForType(Enum type)
        {
            this.type = (ulong)(object)type;
		}

		public override bool Check(int token)
		{
            return TokenManager.GetType(token) == type;
		}
	}
}
