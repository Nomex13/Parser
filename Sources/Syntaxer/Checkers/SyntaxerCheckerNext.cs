﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerCheckerTypeNext : SyntaxerChecker
    {
		private ulong type;

        public SyntaxerCheckerTypeNext(SyntaxerChecker checker, Enum type)
        {
            this.type = (ulong)(object)type;
        }
		public override bool Check(int token)
		{
            return TokenManager.GetType(TokenManager.GetNext(token)) == type;
		}
    }
}
