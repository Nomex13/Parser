﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerSelectionSingle : SyntaxerSelection
    {
        private Enum enumToken;
        private ulong typeToken = 0;

        public SyntaxerSelectionSingle(Enum token)
        {
            enumToken = token;
        }
        protected override void Init()
        {
            typeToken = Syntaxer.Parser.ConvertEnumToTokenId(enumToken);
        }
        public override void Deinit()
        {
            typeToken = 0;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static SyntaxerSelectionSingle Single(this Syntaxer syntaxer, Enum token)
        {
            SyntaxerSelectionSingle selection = new SyntaxerSelectionSingle(token);
            selection.Syntaxer = syntaxer;
            return selection;
        }
    }
}
