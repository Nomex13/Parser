﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryRight : SyntaxerRule
    {
        private Enum enumNew;
        private ulong typeNew = 0;
        private Enum enumOld;
        private ulong typeOld = 0;

        public SyntaxerRuleOperatorUnaryRight(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorUnaryRight(Enum token, Enum tokenMatched)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;

            Match(enumOld);
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.PasteUnder(token, left);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRight(token));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRight(token, tokenMatched));
            return syntaxer;
        }
    }
}
