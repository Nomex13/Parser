﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleGroupBlock : SyntaxerRule
    {
        private Enum enumGroup;
        private ulong typeGroup = 0;
        private string symbolGroup;

        private Enum enumOpen;
        private ulong typeOpen = 0;
        private bool includeOpen;
        private Enum enumClose;
        private ulong typeClose = 0;
        private bool includeClose;

        public SyntaxerRuleGroupBlock(Enum token, Enum tokenMatchedOpening, Enum tokenMatchedClosing)
            : this(token, null, tokenMatchedOpening, tokenMatchedClosing) { }
        public SyntaxerRuleGroupBlock(Enum token, string symbol, Enum tokenMatchedOpening, Enum tokenMatchedClosing)
        {
            this.enumGroup = token;
            this.symbolGroup = symbol;
            this.enumOpen = tokenMatchedOpening;
            this.includeOpen = false;
            this.enumClose = tokenMatchedClosing;
            this.includeClose = false;

            Match(enumOpen);
        }
        protected override void CustomInit()
        {
            typeGroup = Parser.ConvertEnumToTokenId(enumGroup);
            typeOpen  = Parser.ConvertEnumToTokenId(enumOpen);
            typeClose = Parser.ConvertEnumToTokenId(enumClose);
        }
        protected override void CustomDeinit()
        {
            typeGroup = 0;
            typeOpen = 0;
            typeClose = 0;
        }
        public override bool Process(ref int token)
        {
            int depth = 1;
            int tokenCurrent = TokenManager.GetRight(token);
            while (tokenCurrent != 0)
            {
                ulong type = TokenManager.GetType(tokenCurrent);
                if (type == typeOpen)
                {
                    depth++;
                }
                else if (type == typeClose)
                {
                    depth--;
                }
                if (depth == 0)
                {
                    if (symbolGroup != null)
                    {
                        token = TokenManager.Group(typeGroup, symbolGroup, token, includeOpen ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE, tokenCurrent, includeClose ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE);
                    }
                    else
                    {
                        token = TokenManager.Group(typeGroup, token, includeOpen ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE, tokenCurrent, includeClose ? TokenManager.GroupMode.INCLUDE : TokenManager.GroupMode.DELETE);
                    }
                    return true;
                }
                tokenCurrent = TokenManager.GetRight(tokenCurrent);
            }
            var value = TokenManager.GetSymbol(token);
            var lineAndPosition = TokenManager.GetLineAndPosition(token);
            throw new Exception($"Symbol {value} of group {enumGroup} on line {lineAndPosition.Line} at position {lineAndPosition.Position} is unbalanced.");
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Group(this Syntaxer syntaxer, Enum typeGroup, Enum typeOpen, Enum typeClose)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupBlock(typeGroup, typeOpen, typeClose));
            return syntaxer;
        }
        public static Syntaxer Group(this Syntaxer syntaxer, Enum typeGroup, string symbolGroup, Enum typeOpen, Enum typeClose)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupBlock(typeGroup, symbolGroup, typeOpen, typeClose));
            return syntaxer;
        }
    }
}
