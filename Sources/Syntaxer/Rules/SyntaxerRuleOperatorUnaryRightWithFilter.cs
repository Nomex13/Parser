﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryRightWithFilter : SyntaxerRule
    {
        private Enum enumNew;
        private ulong typeNew = 0;
        private Enum enumOld;
        private ulong typeOld = 0;
        private Enum[] enumWhitelistLeft;
        private ulong[] typeWhitelistLeft;
        private Enum[] enumBlacklistLeft;
        private ulong[] typeBlacklistLeft;

        public SyntaxerRuleOperatorUnaryRightWithFilter(Enum token, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
            : this(token, token, tokenWhitelistLeft, tokenBlacklistLeft) { }
        public SyntaxerRuleOperatorUnaryRightWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
            : base(tokenMatched)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;
            this.enumWhitelistLeft = tokenWhitelistLeft?.ToArray() ?? new Enum[0];
            this.enumBlacklistLeft = tokenBlacklistLeft?.ToArray() ?? new Enum[0];
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = Parser.ConvertEnumToTokenId(enumOld);
            typeWhitelistLeft = enumWhitelistLeft.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            typeBlacklistLeft = enumBlacklistLeft.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
            typeWhitelistLeft = null;
            typeBlacklistLeft = null;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            ulong leftType = TokenManager.GetType(left);
            if ((typeWhitelistLeft.Length > 0 && !typeWhitelistLeft.Contains(leftType)) || (typeBlacklistLeft.Length > 0 && typeBlacklistLeft.Contains(leftType)))
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.PasteUnder(token, left);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, params Enum[] tokenWhitelistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, null));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, null));
            return syntaxer;
        }
        public static Syntaxer UnaryRight(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenBlacklistLeft)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryRightWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenBlacklistLeft));
            return syntaxer;
        }
    }
}
