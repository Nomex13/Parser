﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleSplit : SyntaxerRule
    {
        private Enum token;
        private ulong type = 0;
        private Enum groupToken;
        private ulong groupType = 0;
        private string groupSymbol;
        private Enum delimiterToken;
        private ulong delimiterType = 0;
        public SyntaxerRuleSplit(Enum groupToken, Enum delimiterToken)
            : this(groupToken, (string)null, delimiterToken) { }
        public SyntaxerRuleSplit(Enum groupToken, string groupSymbol, Enum delimiterToken)
            : this(TokenBasic.ROOT, groupToken, groupSymbol, delimiterToken) { }
        //public SyntaxerRuleSplit(Enum groupToken, string groupSymbol, Enum delimiterToken)
        //    : base()
        //{
        //    this.groupToken     = groupToken;
        //    this.groupSymbol    = groupSymbol;
        //    this.delimiterToken = delimiterToken;
        //}
        public SyntaxerRuleSplit(Enum token, Enum groupToken, Enum delimiterToken)
            : this(token, groupToken, null, delimiterToken) { }
        public SyntaxerRuleSplit(Enum token, Enum groupToken, string groupSymbol, Enum delimiterToken)
            : base(token)
        {
            this.token          = token;
            this.groupToken     = groupToken;
            this.groupSymbol    = groupSymbol;
            this.delimiterToken = delimiterToken;
        }
        protected override void CustomInit()
        {
            type          = MatchedTypes[0];
            groupType     = Parser.ConvertEnumToTokenId(groupToken);
            delimiterType = Parser.ConvertEnumToTokenId(delimiterToken);
        }
        protected override void CustomDeinit()
        {
            type          = 0;
            groupType     = 0;
            delimiterType = 0;
        }
        public override bool Process(ref int token)
        {
            TokenManager.SplitChildren(token, groupType, null, delimiterType);

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Split(this Syntaxer syntaxer, Enum token, Enum group, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(token, group, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum token, Enum group, string groupSymbol, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(token, group, groupSymbol, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum group, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(group, delimiter));
            return syntaxer;
        }
        public static Syntaxer Split(this Syntaxer syntaxer, Enum group, string groupSymbol, Enum delimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleSplit(group, groupSymbol, delimiter));
            return syntaxer;
        }
    }
}
