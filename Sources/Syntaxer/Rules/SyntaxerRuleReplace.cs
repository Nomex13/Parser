﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleReplace : SyntaxerRule
    {
        private Enum enumNew;
        private ulong typeNew = 0;
        private string symbolNew;
        private Enum enumOld;
        private ulong typeOld = 0;
        private string symbolOld;

        public SyntaxerRuleReplace(Enum token, Enum tokenMatched)
            : this(token, null, tokenMatched, null) { }
        public SyntaxerRuleReplace(Enum token, string symbol, Enum tokenMatched, string symbolMatched)
            : base(tokenMatched)
        {
            this.enumNew = token;
            this.symbolNew = symbol;
            this.enumOld = tokenMatched;
            this.symbolOld = symbolMatched;
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
        }
        public override bool Process(ref int token)
        {
            if (typeOld >= 0 && typeOld != TokenManager.GetType(token))
            {
                return false;
            }
            if (symbolOld != null && symbolOld != TokenManager.GetSymbol(token))
            {
                return false;
            }
            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }
            if (symbolNew != null)
            {
                TokenManager.SetSymbol(token, symbolNew);
            }
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Replace(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(token, tokenMatched));
            return syntaxer;
        }
        public static Syntaxer Replace(this Syntaxer syntaxer, string symbol, string symbolMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(null, symbol, null, symbolMatched));
            return syntaxer;
        }
        public static Syntaxer Replace(this Syntaxer syntaxer, Enum token, string symbol, Enum tokenMatched, string symbolMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplace(token, symbol, tokenMatched, symbolMatched));
            return syntaxer;
        }
    }
}
