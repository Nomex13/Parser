﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleGroupAround : SyntaxerRule
    {
        private Enum enumGroup;
        private ulong typeGroup = 0;
        private string symbolGroup;
        private Enum enumToken;
        private ulong typeToken;

        private int leftCount;
        private int rightCount;

        public SyntaxerRuleGroupAround(Enum token, int leftCount, int rightCount)
            : base(token)
        {
            this.enumGroup   = null;
            this.symbolGroup = null;

            this.enumToken  = token;
            this.leftCount  = leftCount;
            this.rightCount = rightCount;
        }
        public SyntaxerRuleGroupAround(Enum token, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
            : this(tokenMatched, tokensLeftCount, tokensRightCount)
        {
            this.enumGroup = token;
            this.symbolGroup = null;
        }
        public SyntaxerRuleGroupAround(Enum token, string symbol, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
            : this(tokenMatched, tokensLeftCount, tokensRightCount)
        {
            this.enumGroup = token;
            this.symbolGroup = symbol;
        }
        protected override void CustomInit()
        {
            typeGroup = Parser.ConvertEnumToTokenId(enumGroup);
            typeToken = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            typeGroup = 0;
            typeToken = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token, leftCount);
            if (left == 0)
            {
                return false;
            }
            int right = TokenManager.GetRight(token, rightCount);
            if (right == 0)
            {
                return false;
            }

            token = TokenManager.GroupAround(typeGroup, symbolGroup, token, leftCount, rightCount); // Redo

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Group(this Syntaxer syntaxer, Enum token, Enum tokenMatched, int tokensLeftCount, int tokensRightCount)
        {
            syntaxer.AddRule(new SyntaxerRuleGroupAround(token, tokenMatched, tokensLeftCount, tokensRightCount));
            return syntaxer;
        }
        //public static Syntaxer OperatorUnary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 0, 1));
        //    return syntaxer;
        //}
        //public static Syntaxer OperatorBinary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 1, 1));
        //    return syntaxer;
        //}
        //public static Syntaxer OperatorTernary(this Syntaxer syntaxer, Enum type)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleGroupAround(type, 1, 2));
        //    return syntaxer;
        //}
    }
}
