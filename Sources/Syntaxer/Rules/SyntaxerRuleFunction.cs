﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleFunction : SyntaxerRule
    {
        private Enum enumFunction;
        private ulong typeFunction = 0;
        private Enum enumArgument;
        private ulong typeArgument = 0;
        //private string symbolArgument;
        private Enum enumMatchFunctionName;
        private ulong typeMatchFunctionName = 0;
        private Enum enumMatchFunctionArguments;
        private ulong typeMatchFunctionArguments = 0;
        private Enum enumMatchFunctionArgumentsDelimiter;
        private ulong typeMatchFunctionArgumentsDelimiter = 0;

        public SyntaxerRuleFunction(Enum tokenFunction, Enum tokenArgument, Enum functionName, Enum functionArguments, Enum functionArgumentsDelimiter)
        //    : this(token, functionName, functionarguments, functionArgumentsDelimiter) { }
        //public SyntaxerRuleFunction(Enum function, Enum argument, string argumentSymbol, Enum name, Enum arguments, Enum delimiter)
            : base(functionName)
        {
            this.enumFunction = tokenFunction;
            this.enumArgument = tokenArgument;
            //this.symbolArgument = argumentSymbol;
            this.enumMatchFunctionName       = functionName;
            this.enumMatchFunctionArguments  = functionArguments;
            this.enumMatchFunctionArgumentsDelimiter = functionArgumentsDelimiter;
        }
        protected override void CustomInit()
        {
            typeFunction                        = Parser.ConvertEnumToTokenId(enumFunction);
            typeArgument                        = Parser.ConvertEnumToTokenId(enumArgument);
            typeMatchFunctionName               = MatchedTypes[0];
            typeMatchFunctionArguments          = Parser.ConvertEnumToTokenId(enumMatchFunctionArguments);
            typeMatchFunctionArgumentsDelimiter = Parser.ConvertEnumToTokenId(enumMatchFunctionArgumentsDelimiter);
        }
        protected override void CustomDeinit()
        {
            typeFunction                        = 0;
            typeArgument                        = 0;
            typeMatchFunctionName               = 0;
            typeMatchFunctionArguments          = 0;
            typeMatchFunctionArgumentsDelimiter = 0;
        }
        public override bool Process(ref int token)
        {
            int tokenArguments = TokenManager.GetRight(token);
            if (typeMatchFunctionArguments != TokenManager.GetType(tokenArguments))
            {
                return false;
            }

            // Leave only the token with arguments underneath it
            //string symbolFunction = TokenManager.GetSymbol(token);
            //TokenManager.Delete(token);

            //TokenManager.DeleteChildrenOfType(tokenArguments, typeFunctionArgumentsDelimiter);
            //TokenManager.MoveChildren(token, tokenArguments);
            //TokenManager.Delete(tokenArguments);
            //TokenManager.SetType(token, typeToken);

            // Split the arguments
            TokenManager.SplitChildren(tokenArguments, typeArgument, typeMatchFunctionArgumentsDelimiter);

            // Merge arguments into the function
            token = TokenManager.Merge(token, tokenArguments);
            //token = TokenManager.Merge(typeFunction, TokenManager.GetSymbol(token), token, tokenArguments);

            // Set the name and the type of the function
            TokenManager.SetType(token, typeFunction);

            //token = tokenArguments;
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Function(this Syntaxer syntaxer, Enum tokenFunction, Enum tokenArgument, Enum functionName, Enum functionArguments, Enum functionArgumentsDelimiter)
        {
            syntaxer.AddRule(new SyntaxerRuleFunction(tokenFunction, tokenArgument, functionName, functionArguments, functionArgumentsDelimiter));
            return syntaxer;
        }
        //public static Syntaxer Function(this Syntaxer syntaxer, Enum function, Enum argument, string argumentSymbol, Enum name, Enum arguments, Enum delimiter)
        //{
        //    syntaxer.AddRule(new SyntaxerRuleFunction(function, argument, argumentSymbol, name, arguments, delimiter));
        //    return syntaxer;
        //}
    }
}
