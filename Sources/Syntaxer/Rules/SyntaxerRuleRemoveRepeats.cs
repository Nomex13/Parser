﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleRemoveRepeats : SyntaxerRule
    {
        private Enum @enum;
        private ulong type = 0;
        private int countMaximum;
        public SyntaxerRuleRemoveRepeats(Enum token, int countMaximum = 1)
        {
            this.@enum = token;
            this.countMaximum = countMaximum;

            Match(@enum);
        }
        protected override void CustomInit()
        {
            type = Parser.ConvertEnumToTokenId(@enum);
        }
        protected override void CustomDeinit()
        {
            type = 0;
        }
        public override bool Process(ref int token)
        {
            int current = TokenManager.GetNext(token);
            // Skip necessary count of tokens while the type matches
            int count = 1;
            while (count < countMaximum)
            {
                current = TokenManager.GetNext(current);
                if (TokenManager.GetType(current) != type)
                {
                    return false;
                }
                count++;
            }
            // Delete all following tokens while the type matches
            if (TokenManager.GetType(current) != type)
            {
                while (true)
                {
                    int next = TokenManager.GetNext(current);
                    TokenManager.Delete(current);
                    current = next;
                    // Check the type still matches
                    if (TokenManager.GetType(current) != type)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer RemoveRepeats(this Syntaxer syntaxer, Enum token, int countMaximum = 1)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveRepeats(token, countMaximum));
            return syntaxer;
        }
    }
}
