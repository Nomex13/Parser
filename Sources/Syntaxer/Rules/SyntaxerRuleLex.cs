﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleLex : SyntaxerRule
    {
        private Lexer lexer;

        public SyntaxerRuleLex(Lexer lexer)
        {
            this.lexer = lexer;
        }
        public SyntaxerRuleLex(Enum token, Lexer lexer)
            : base(token)
        {
            this.lexer = lexer;
        }
        protected override void CustomInit()
        {
            lexer.Initialize(Parser);
        }
        protected override void CustomDeinit()
        {
            lexer.Deinitialize();
        }
        public override bool Process(ref int token)
        {
            lexer.Process(token);

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Lex(this Syntaxer syntaxer, Lexer lexer)
        {
            syntaxer.AddRule(new SyntaxerRuleLex(lexer));
            return syntaxer;
        }
    }
}
