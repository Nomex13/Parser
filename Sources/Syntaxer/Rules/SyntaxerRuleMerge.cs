﻿using System;

namespace Iodynis.Libraries.Parsing
{
    /// <summary>
    /// Replaces a squence of tokens of the same type with only one.
    /// Concatenates their symbols.
    /// </summary>
    public class SyntaxerRuleMerge : SyntaxerRule
    {
        private readonly Enum @enum;
        private ulong type;

        public SyntaxerRuleMerge(Enum token)
            : base(token)
        {
            @enum = token;
        }
        protected override void CustomInit()
        {
            type = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            type = 0;
        }
        public override bool Process(ref int token)
        {
            int tokenStart = token;
            int tokenEnd = token;
            {
                int tokenCurrent = TokenManager.GetRight(token);
                while (TokenManager.GetType(tokenCurrent) == type)
                {
                    tokenEnd = tokenCurrent;
                    tokenCurrent = TokenManager.GetRight(tokenCurrent);
                }
            }
            // If only one token is presented then there is nothing to merge
            if (tokenStart == tokenEnd)
            {
                return false;
            }

            token = TokenManager.Merge(type, tokenStart, tokenEnd);
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        /// <summary>
        /// Merge seqential tokens of one type into one token of that type.
        /// </summary>
        /// <param name="syntaxer"></param>
        /// <param name="token">Token type to merge.</param>
        /// <returns></returns>
        public static Syntaxer Merge(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleMerge(token));
            return syntaxer;
        }
    }
}
