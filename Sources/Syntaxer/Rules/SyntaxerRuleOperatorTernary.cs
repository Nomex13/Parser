﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorTernary : SyntaxerRule
    {
        private readonly Enum enumToken;
        private ulong typeToken = 0;
        private readonly Enum enumOperator;
        private ulong typeOperator = 0;
        private readonly Enum enumOperatorPartOne;
        private ulong typeOperatorPartOne = 0;
        private readonly Enum enumOperatorPartTwo;
        private ulong typeOperatorPartTwo = 0;

        public SyntaxerRuleOperatorTernary(Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
            : this(tokenOperator, tokenOperator, tokenOperatorPartOne, tokenOperatorPartTwo) { }
        public SyntaxerRuleOperatorTernary(Enum token, Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
            : base(tokenOperatorPartOne)
        {
            this.enumToken = token;
            this.enumOperator = tokenOperator;
            this.enumOperatorPartOne = tokenOperatorPartOne;
            this.enumOperatorPartTwo = tokenOperatorPartTwo;
        }
        protected override void CustomInit()
        {
            typeToken = Parser.ConvertEnumToTokenId(enumToken);
            typeOperator = Parser.ConvertEnumToTokenId(enumOperator);
            typeOperatorPartOne = MatchedTypes[0];
            typeOperatorPartTwo = Parser.ConvertEnumToTokenId(enumOperatorPartTwo);
        }
        protected override void CustomDeinit()
        {
            typeToken = 0;
            typeOperator = 0;
            typeOperatorPartOne = 0;
            typeOperatorPartTwo = 0;
        }
        public override bool Process(ref int token)
        {
            int leftOperand = TokenManager.GetLeft(token);
            if (leftOperand == 0)
            {
                return false;
            }
            int leftOperator = token;
            int middleOperand = TokenManager.GetRight(token);
            if (middleOperand == 0)
            {
                return false;
            }
            int rightOperator = TokenManager.GetRight(middleOperand);
            if (rightOperator == 0)
            {
                return false;
            }
            int rightOperand = TokenManager.GetRight(rightOperator);
            if (rightOperand == 0)
            {
                return false;
            }

            TokenManager.Delete(rightOperator);
            TokenManager.GroupAround(typeOperator, TokenManager.GetSymbol(leftOperator) + TokenManager.GetSymbol(rightOperator), leftOperator, 1, 2);

            if (typeToken >= 0)
            {
                TokenManager.SetType(token, typeToken);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Ternary(this Syntaxer syntaxer, Enum token, Enum tokenOperator, Enum tokenOperatorPartOne, Enum tokenOperatorPartTwo)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorTernary(token, tokenOperator, tokenOperatorPartOne, tokenOperatorPartTwo));
            return syntaxer;
        }
    }
}
