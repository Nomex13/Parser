﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorBinaryWithFilter : SyntaxerRule
    {
        private readonly Enum enumNew;
        private ulong typeNew = 0;
        private readonly Enum enumOld;
        private ulong typeOld = 0;
        private readonly Enum[] enumWhitelistLeft;
        private ulong[] typeWhitelistLeft;
        private readonly Enum[] enumBlacklistLeft;
        private ulong[] typeBlacklistLeft;
        private readonly Enum[] enumWhitelistRight;
        private ulong[] typeWhitelistRight;
        private readonly Enum[] enumBlacklistRight;
        private ulong[] typeBlacklistRight;

        public SyntaxerRuleOperatorBinaryWithFilter(Enum token, IEnumerable<Enum> tokenWwhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
            : this(token, token, tokenWwhitelistLeft, tokenWhitelistRight, tokenBlacklistLeft, tokenBlacklistRight) { }
        public SyntaxerRuleOperatorBinaryWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
            : base(token)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;
            this.enumWhitelistLeft  = tokenWhitelistLeft?.ToArray()  ?? new Enum[0];
            this.enumBlacklistLeft  = tokenBlacklistLeft?.ToArray()  ?? new Enum[0];
            this.enumWhitelistRight = tokenWhitelistRight?.ToArray() ?? new Enum[0];
            this.enumBlacklistRight = tokenBlacklistRight?.ToArray() ?? new Enum[0];
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = MatchedTypes[0];
            typeWhitelistLeft  = enumWhitelistLeft .Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            typeBlacklistLeft  = enumBlacklistLeft .Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            typeWhitelistRight = enumWhitelistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            typeBlacklistRight = enumBlacklistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
            typeWhitelistLeft  = null;
            typeBlacklistLeft  = null;
            typeWhitelistRight = null;
            typeBlacklistRight = null;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            ulong leftType = TokenManager.GetType(left);
            if ((typeWhitelistLeft.Length > 0 && !typeWhitelistLeft.Contains(leftType)) || (typeBlacklistLeft.Length > 0 && typeBlacklistLeft.Contains(leftType)))
            {
                return false;
            }

            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }
            ulong rightType = TokenManager.GetType(right);
            if ((typeWhitelistRight.Length > 0 && !typeWhitelistRight.Contains(rightType)) || (typeBlacklistRight.Length > 0 && typeBlacklistRight.Contains(rightType)))
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.Cut(right);

            TokenManager.PasteFirstUnder(token, left);
            TokenManager.PasteLastUnder(token, right);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, Enum tokenWhitelistLeft, Enum tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, new Enum[] { tokenWhitelistLeft }, new Enum[] { tokenWhitelistRight }, null, null));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenWhitelistRight, null, null));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistLeft, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistLeft, IEnumerable<Enum> tokenBlacklistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinaryWithFilter(token, tokenMatched, tokenWhitelistLeft, tokenWhitelistRight, tokenBlacklistLeft, tokenBlacklistRight));
            return syntaxer;
        }
    }
}
