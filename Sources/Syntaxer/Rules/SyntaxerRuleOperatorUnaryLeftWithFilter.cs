﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryLeftWithFilter : SyntaxerRule
    {
        private Enum enumNew;
        private ulong typeNew = 0;
        private Enum enumOld;
        private ulong typeOld = 0;
        private Enum[] enumWhitelistRight;
        private ulong[] typeWhitelistRight;
        private Enum[] enumBlacklistRight;
        private ulong[] typeBlacklistRight;

        public SyntaxerRuleOperatorUnaryLeftWithFilter(Enum token, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
            : this(token, token, tokenWhitelistRight, tokenBlacklistRight) { }
        public SyntaxerRuleOperatorUnaryLeftWithFilter(Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;
            this.enumWhitelistRight = tokenWhitelistRight?.ToArray() ?? new Enum[0];
            this.enumBlacklistRight = tokenBlacklistRight?.ToArray() ?? new Enum[0];

            Match(enumOld);
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = Parser.ConvertEnumToTokenId(enumOld);
            typeWhitelistRight = enumWhitelistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
            typeBlacklistRight = enumBlacklistRight.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
            typeWhitelistRight = null;
            typeBlacklistRight = null;
        }
        public override bool Process(ref int token)
        {
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }
            ulong rightType = TokenManager.GetType(right);
            if ((typeWhitelistRight.Length > 0 && !typeWhitelistRight.Contains(rightType)) || (typeBlacklistRight.Length > 0 && typeBlacklistRight.Contains(rightType)))
            {
                return false;
            }

            TokenManager.Cut(right);
            TokenManager.PasteUnder(token, right);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, params Enum[] tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, null));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, null));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched, IEnumerable<Enum> tokenWhitelistRight, IEnumerable<Enum> tokenBlacklistRight)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeftWithFilter(token, tokenMatched, tokenWhitelistRight, tokenBlacklistRight));
            return syntaxer;
        }
    }
}
