﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorUnaryLeft : SyntaxerRule
    {
        private readonly Enum enumNew;
        private ulong typeNew = 0;
        private readonly Enum enumOld;
        private ulong typeOld = 0;

        public SyntaxerRuleOperatorUnaryLeft(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorUnaryLeft(Enum token, Enum tokenMatched)
            : base(tokenMatched)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }

            TokenManager.Cut(right);
            TokenManager.PasteUnder(token, right);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeft(token));
            return syntaxer;
        }
        public static Syntaxer UnaryLeft(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorUnaryLeft(token, tokenMatched));
            return syntaxer;
        }
    }
}
