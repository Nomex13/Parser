﻿using System;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleOperatorBinary : SyntaxerRule
    {
        private readonly Enum enumNew;
        private ulong typeNew = 0;
        private readonly Enum enumOld;
        private ulong typeOld = 0;

        public SyntaxerRuleOperatorBinary(Enum token)
            : this(token, token) { }
        public SyntaxerRuleOperatorBinary(Enum token, Enum tokenMatched)
            : base(tokenMatched)
        {
            this.enumNew = token;
            this.enumOld = tokenMatched;
        }
        protected override void CustomInit()
        {
            typeNew = Parser.ConvertEnumToTokenId(enumNew);
            typeOld = MatchedTypes[0];
        }
        protected override void CustomDeinit()
        {
            typeNew = 0;
            typeOld = 0;
        }
        public override bool Process(ref int token)
        {
            int left = TokenManager.GetLeft(token);
            if (left == 0)
            {
                return false;
            }
            int right = TokenManager.GetRight(token);
            if (right == 0)
            {
                return false;
            }

            TokenManager.Cut(left);
            TokenManager.Cut(right);

            TokenManager.PasteFirstUnder(token, left);
            TokenManager.PasteLastUnder(token, right);

            if (typeNew >= 0)
            {
                TokenManager.SetType(token, typeNew);
            }

            token = right;

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinary(token));
            return syntaxer;
        }
        public static Syntaxer Binary(this Syntaxer syntaxer, Enum token, Enum tokenMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleOperatorBinary(token, tokenMatched));
            return syntaxer;
        }
    }
}
