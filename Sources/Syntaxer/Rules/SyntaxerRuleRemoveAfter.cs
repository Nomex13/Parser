﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleRemoveAfter : SyntaxerRule
    {
        private Enum[] tokensToRemove;
        private ulong[] typesToRemove;
        public SyntaxerRuleRemoveAfter(Enum token, Enum tokenToRemove)
            : this(token, new Enum[] { tokenToRemove }) { }
        public SyntaxerRuleRemoveAfter(Enum token, IEnumerable<Enum> tokensToRemove)
            : this(token, tokensToRemove?.ToArray()) { }
        public SyntaxerRuleRemoveAfter(Enum token, params Enum[] tokensToRemove)
            : base(token)
        {
            if (tokensToRemove == null)
            {
                throw new ArgumentNullException(nameof(tokensToRemove));
            }
            this.tokensToRemove = tokensToRemove;
        }
        protected override void CustomInit()
        {
            typesToRemove = tokensToRemove.Select(_enum => Parser.ConvertEnumToTokenId(_enum)).ToArray();
        }
        protected override void CustomDeinit()
        {
            typesToRemove = null;
        }
        public override bool Process(ref int token)
        {
            int current = TokenManager.GetRight(token);
            while (current != 0 && typesToRemove.Contains(TokenManager.GetType(current)))
            {
                TokenManager.Delete(current);
            }
            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer RemoveAfter(this Syntaxer syntaxer, Enum token, Enum tokenToRemove)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveAfter(token, tokenToRemove));
            return syntaxer;
        }
        public static Syntaxer RemoveAfter(this Syntaxer syntaxer, Enum token, IEnumerable<Enum> tokensToRemove)
        {
            syntaxer.AddRule(new SyntaxerRuleRemoveAfter(token, tokensToRemove));
            return syntaxer;
        }
    }
}
