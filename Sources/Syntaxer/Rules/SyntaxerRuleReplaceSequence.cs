﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Parsing
{
    public class SyntaxerRuleReplaceSequence : SyntaxerRule
    {
        private int countNew = 0;
        private int countOld = 0;
        private List<Enum> enumsNew = new List<Enum>();
        private List<ulong> typesNew = null;
        private List<string> symbolsNew = new List<string>();
        private List<Enum> enumsOld = new List<Enum>();
        private List<ulong> typesOld = null;
        private List<string> symbolsOld = new List<string>();

        //public SyntaxerRuleReplaceSequence(Enum token, params Enum[] match)
        //    : this(token, null, match, null) { }
        public SyntaxerRuleReplaceSequence(IEnumerable<Enum> tokens, IEnumerable<Enum> tokensMatched)
        {
            if (tokensMatched.Count() == 0)
            {
                throw new Exception("Cannot replace an empty sequence. The sequence should contain at least one token.");
            }

            this.countNew = tokens.Count();
            this.countOld = tokensMatched.Count();
            this.enumsNew.AddRange(tokens);
            this.symbolsNew.AddRange(tokens.Select(_ => (string)null));
            this.enumsOld.AddRange(tokensMatched);
            this.symbolsOld.AddRange(tokensMatched.Select(_ => (string)null));

            Match(enumsOld[0]);
        }
        public SyntaxerRuleReplaceSequence(IEnumerable<(Enum Token, string Symbol)> tokensAndSymbols, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            if (tokensAndSymbolsMatched.Count() == 0)
            {
                throw new Exception("Cannot replace an empty sequence. The sequence should contain at least one token.");
            }

            this.countNew = tokensAndSymbols.Count();
            this.countOld = tokensAndSymbolsMatched.Count();
            this.enumsNew.AddRange(tokensAndSymbols.Select(_new => _new.Token));
            this.symbolsNew.AddRange(tokensAndSymbols.Select(_new => _new.Symbol));
            this.enumsOld.AddRange(tokensAndSymbolsMatched.Select(_old => _old.Token));
            this.symbolsOld.AddRange(tokensAndSymbolsMatched.Select(_old => _old.Symbol));

            Match(enumsOld[0]);
        }
        protected override void CustomInit()
        {
            this.typesNew = new List<ulong>(enumsNew.Select(_enum => Parser.ConvertEnumToTokenId(_enum)));
            this.typesOld = new List<ulong>(enumsOld.Select(_enum => Parser.ConvertEnumToTokenId(_enum)));
        }
        protected override void CustomDeinit()
        {
            this.typesNew = null;
            this.typesOld = null;
        }
        public override bool Process(ref int token)
        {
            int oldTokenLast = token;

            // Check tokens do match
            // And get the last token
            {
                int oldTokenCurrent = token;
                for (int i = 0; i < countOld; i++)
                {
                    if (oldTokenCurrent == 0)
                    {
                        return false;
                    }
                    if (typesOld[i] >= 0 && typesOld[i] != TokenManager.GetType(oldTokenCurrent))
                    {
                        return false;
                    }
                    if (symbolsOld[i] != null && symbolsOld[i] != TokenManager.GetSymbol(oldTokenCurrent))
                    {
                        return false;
                    }
                    oldTokenLast = oldTokenCurrent;
                    oldTokenCurrent = TokenManager.GetRight(oldTokenCurrent);
                }
            }

            int start = TokenManager.GetTextStart(token);
            int stop = TokenManager.GetTextEnd(oldTokenLast);

            {
                int tokenCurrent = token;
                for (int i = 0; i < countNew; i++)
                {
                    // Replace an existing token
                    if (i < countOld)
                    {
                        TokenManager.Set(tokenCurrent, typesNew[i], symbolsNew[i], start, stop);
                        TokenManager.DeleteChildren(tokenCurrent);
                        tokenCurrent = TokenManager.GetRight(tokenCurrent);
                    }
                    // Add a new token
                    else
                    {
                        int tokenNew = TokenManager.Create(typesNew[i], symbolsNew[i], start, stop);
                        TokenManager.PasteRight(tokenCurrent, tokenNew);
                        tokenCurrent = tokenNew;
                    }
                }
                // Remove old tokens if any left
                if (countNew < countOld)
                {
                    TokenManager.DeleteBlock(TokenManager.GetRight(tokenCurrent), oldTokenLast);
                }
            }

            return true;
        }
    }
    public static partial class SyntaxerExtensions
    {
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbols, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(tokensAndSymbols, tokensAndSymbolsMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, IEnumerable<Enum> tokens, IEnumerable<Enum> tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(tokens, tokensMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, (Enum Token, string Symbol) tokensAndSymbol, IEnumerable<(Enum Token, string Symbol)> tokensAndSymbolsMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new (Enum Token, string Symbol)[] { tokensAndSymbol }, tokensAndSymbolsMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, Enum token, IEnumerable<Enum> tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new Enum[] { token }, tokensMatched));
            return syntaxer;
        }
        public static Syntaxer ReplaceSequence(this Syntaxer syntaxer, Enum token, params Enum[] tokensMatched)
        {
            syntaxer.AddRule(new SyntaxerRuleReplaceSequence(new Enum[] { token }, tokensMatched));
            return syntaxer;
        }
    }
}
