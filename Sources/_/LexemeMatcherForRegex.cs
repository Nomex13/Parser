﻿//using System;
//using System.Text.RegularExpressions;

//namespace Iodynis.Libraries.Parsing
//{
//	public class LexemeMatcherForRegex : LexemeMatcher
//	{
//		public Regex Regex { get; }
//		public bool HasRegexGroupPrefix { get; }
//		public bool HasRegexGroupMatch { get; }
//		public bool HasRegexGroupPostfix { get; }

//		private LexemeMatcherForRegex(string symbol)
//			: base()
//		{
//			if (String.IsNullOrEmpty(symbol))
//			{
//				throw new Exception("Symbol cannot be null or empty.");
//			}
//			Regex = new Regex(@"\G" /* Means search should start from specified index in Match function */ + symbol);
//			HasRegexGroupPrefix  = Regex != null && Regex.GroupNumberFromName("prefix")  >= 0;
//			HasRegexGroupMatch   = Regex != null && Regex.GroupNumberFromName("match")   >= 0;
//			HasRegexGroupPostfix = Regex != null && Regex.GroupNumberFromName("postfix") >= 0;
//		}

//		public override bool Match(string text, int index, out LexemeMatch match)
//		{
//            match = null;

//			if (text == null)
//			{
//				return false;
//			}

//			//System.Text.RegularExpressions.Match match = field_regex.Match(param_string, param_index);
//			//return match.Success ? (field_hasRegexGroupMatch ? match.Groups["match"].Value.Length : match.Value.Length) : 0;
//			Match regexMatch = Regex.Match(text, index);
//			if (regexMatch.Success)
//			{
//				//return field_hasRegexGroupMatch ? match.Groups["match"].Value : match.Value;
//				match = new LexemeMatch(this, HasRegexGroupMatch ? regexMatch.Groups["match"].Value : regexMatch.Value, index, index + regexMatch.Length);
//			    return true;

//					//param_index + match.Groups["start"].Length + match.Groups["match"].Length + (field_includeEnd ? match.Groups["end"].Length : 0));
//					//param_index + match.Groups["start"].Length + match.Groups["match"].Length + match.Groups["end"].Length);
//					//param_index + (field_hasRegexGroupPrefix ? match.Groups["start"].Length : 0) + (field_hasRegexGroupMatch ? match.Groups["match"].Length : 0) + (field_hasRegexGroupPostfix ? match.Groups["end"].Length : 0));
//			}

//			return false;
//		}

//		public override string ToString()
//		{
//			return Regex.ToString();
//		}
//	}
//}
