﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;

//namespace Iodynis.Libraries.Parsing
//{
//	public class LexemeOperatorAdd : LexemeOperator
//	{
//		private readonly List<TokenType> tokenTypes = new List<TokenType>();

//		//public LexemeOperatorAdd(TokenType tokenType)
//		//	: this(tokenType) { }
//		public LexemeOperatorAdd(TokenType tokenType)
//			: base()
//		{
//			tokenTypes.Add(tokenType);
//		}
//		public LexemeOperatorAdd(IEnumerable<TokenType> tokenTypes)
//			//: this(tokenTypes) { }
//		//private LexemeOperatorAdd(IEnumerable<TokenType> tokenTypes)
//			: base()
//		{
//			this.tokenTypes.AddRange(tokenTypes);
//		}

//		public override Token Operate(Token token = null)
//		{
//			LexemeMatch match = Rule.Matcher.Match;
//			for (int i = 0; i < tokenTypes.Count; i++)
//			{
//				if (token == null)
//				{
//					token = new Token(tokenTypes[i], match.Symbol, match.IndexOfStart, match.IndexOfStop);
//				}
//				else
//				{
//					token = token.AddNext(new Token(tokenTypes[i], match.Symbol, match.IndexOfStart, match.IndexOfStop));
//				}
//			}
//			return token;
//		}
//	}
//}
