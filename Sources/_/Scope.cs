﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Iodynis
//{
//	public class Scope
//	{
//		private int field_typesCounter = 1000;
//		private Dictionary<string, int> field_types = new Dictionary<string, int>();
//		private Dictionary<string, object> field_variables = new Dictionary<string, object>();
//		private Scope field_parentScope;

//		public Scope(Scope param_parentScope = null)
//		{
//			field_parentScope = param_parentScope;
//		}

//		public int GetType(Type param_type)
//		{
//			if (!field_types.ContainsKey(param_type.FullName))
//			{
//				field_types.Add(param_type.FullName, field_typesCounter++);
//			}
//			return field_types[param_type.FullName];
//		}

//		public object GetVariable(object param_name)
//		{
//			string name = param_name.ToString();
//			if (field_variables.ContainsKey(name))
//			{
//				return field_variables[name];
//			}
//			else if (field_parentScope != null)
//			{
//				return field_parentScope.GetVariable(param_name);
//			}
//			else
//			{
//				return null;
//			}
//		}
//		public object SetVariable(object param_name, object param_value)
//		{
//			string name = param_name.ToString();
//			if (field_variables.ContainsKey(name))
//			{
//				field_variables[name] = param_value;
//			}
//			else
//			{
//				field_variables.Add(name, param_value);
//			}
//			return field_variables[name];
//		}
//	}
//}
