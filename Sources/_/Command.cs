﻿//using System;
//using System.Collections.Generic;

//namespace Iodynis
//{
//    public abstract class Command
//    {
//        private static int field_counter = 0;
//        private int field_id;
//        public int Id { get { return field_id; } }
//        protected String field_symbol;
//        public String Symbol { get { return field_symbol; } }
//        private Command()
//        {
//            field_id = field_counter++;
//        }
//        protected Command(String symbol)
//            : this()
//        {
//            field_symbol = symbol;
//        }
//        /// <summary>
//        /// Executes logic contained in the object.
//        /// Due to consecutive execution multiple calls to this method may be necessary
//        /// </summary>
//        /// <returns>True if done, false if not and needs more iterations of execution</returns>
//        public virtual bool Do()
//        {
//            return true;
//        }
//        // TODO: Undo logic for all commands
//        public virtual bool Undo()
//        {
//            return true;
//        }
//    }
//    public class CommandBlank : Command
//    {
//        public CommandBlank()
//            : base("blank") { }
//    }
//}
