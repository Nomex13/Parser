﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Parser
//{
//    public class Variable
//    {
//        private string field_name;
//        public string Name { get { return field_name; } }
//        protected object field_value;
//        //public object Value { get { return field_value; } set { field_value = value; } }

//        protected Variable(string param_name, object param_value = null)
//        {
//            field_name = param_name;
//            field_value = param_value;
//        }
//    }
//    public class VariableNumber : Variable
//    {
//        //private float field_value;
//        public float Value { get { return (float)field_value; } set { field_value = value; } }

//        public VariableNumber(string param_name, float param_value = 0)
//            : base(param_name)
//        {
//            field_value = param_value;
//        }
//    }
//    public class VariableString : Variable
//    {
//        //private string field_value;
//        public string Value { get { return (string)field_value; } set { field_value = value; } }

//        public VariableString(string param_name, string param_value = "")
//            : base(param_name)
//        {
//            field_value = param_value;
//        }
//    }
//    public class VariableRole : Variable
//    {
//        //private ScenarioRole field_value;
//        public ScenarioRole Value { get { return (ScenarioRole)field_value; } set { field_value = value; } }

//        public VariableRole(string param_name, ScenarioRole param_value = null)
//            : base(param_name)
//        {
//            field_value = param_value ?? new ScenarioRole(param_name);
//        }
//    }
//}
