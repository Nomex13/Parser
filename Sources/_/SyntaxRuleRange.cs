﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Namespace
//{
//	public class SyntaxRuleRange : SyntaxRule
//	{
//		private TokenType field_type;
//		public TokenType Type { get { return field_type; } }
//		private string field_symbol;
//		public string Symbol { get { return field_symbol; } }

//		private SyntaxRule field_syntaxRuleStart;
//		public SyntaxRule SyntaxRuleStart { get { return field_syntaxRuleStart; } }
//		private SyntaxRule field_syntaxRuleStop;
//		public SyntaxRule SyntaxRuleStop { get { return field_syntaxRuleStop; } }
//		private bool field_isNestingAllowed = false;
//		public bool IsNestingAllowed { get { return field_isNestingAllowed; } }

//		public SyntaxRuleRange(TokenType param_type, string param_symbol, SyntaxRule param_syntaxRuleStart, SyntaxRule param_syntaxRuleStop, bool param_isNestingAllowed = false)
//			: base()
//		{
//			field_type = param_type;
//			field_symbol = param_symbol;
//			field_syntaxRuleStart = param_syntaxRuleStart;
//			field_syntaxRuleStop = param_syntaxRuleStop;
//			field_isNestingAllowed = param_isNestingAllowed;
//		}

//		//public override SyntaxMatch Match(Token param_token)//, int param_index)
//		//{
//		//	//int index = param_index;
//		//	return MatchRecursion(param_tokens, ref index);
//		//}

//		//private SyntaxMatch MatchRecursion(List<Token> param_tokens, ref int param_index)
//		//{
//		//	if (param_index < 0 || param_index > param_tokens.Count - 2 /* Need a closing token too */)
//		//	{
//		//		return null;
//		//	}

//		//	// Catch the opening
//		//	SyntaxMatch syntaxMatchStart = field_syntaxRuleStart.Match(param_tokens, param_index);
//		//	if (syntaxMatchStart == null)
//		//	{
//		//		return null;
//		//	}

//		//	int syntaxMatchStartIndex = param_index;
//		//	int syntaxMatchStopIndex = -1;

//		//	// Catch the closing
//		//	SyntaxMatch syntaxMatchStop = null;
//		//	while (param_index < param_tokens.Count)
//		//	{
//		//		// Look for an opening if nesting is allowed
//		//		if (field_isNestingAllowed)
//		//		{
//		//			if (field_syntaxRuleStart.Match(param_tokens, param_index) != null)
//		//			{
//		//				MatchRecursion(param_tokens, ref param_index);
//		//				continue;
//		//			}
//		//		}

//		//		// Check for closing
//		//		syntaxMatchStop = field_syntaxRuleStop.Match(param_tokens, param_index);
//		//		if (syntaxMatchStop != null)
//		//		{
//		//			syntaxMatchStopIndex = param_index;
//		//			return new SyntaxMatch(this, new Token(field_type, field_symbol, param_tokens.GetRange(syntaxMatchStartIndex + 1, syntaxMatchStopIndex - syntaxMatchStartIndex - 1)));
//		//		}

//		//		param_index++;
//		//	}

//		//	return null;
//		//}
//	}
//}
