﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Iodynis.Libraries.Parsing
//{
//	public class Token
//	{
//		[Flags]
//		public enum SearchDirection
//		{
//			RIGHT = 1,
//			LEFT = 2,
//			UP = 4,
//			DOWN = 8,
//		}
//		public Enum Type;
//		public string Symbol;
//		//public readonly int SourceStartIndex;
//		//public readonly int SourceStopIndex;

//        // Hierarchy
//		private Token up;
//        public Token Up { get { return up; } }
//		private Token right;
//        public Token Right { get { return right; } }
//		private Token left;
//        public Token Left { get { return left; } }
//		private Token down;
//        public Token Down { get { return down; } }

//		public Token(Enum type, string symbol, Token child)
//		{
//			if (child == null)
//            {
//                throw new ArgumentNullException(nameof(child));
//            }
//            Type = type;
//			Symbol = symbol;
//			AddDown(child);
//		}
//		public Token(Enum type, string symbol, List<Token> children)
//		{
//			if (children == null)
//            {
//                throw new ArgumentNullException(nameof(children));
//            }
//            Type = type;
//			Symbol = symbol;
//			for (int i = 0; i < children.Count; i++)
//			{
//				AddDown(children[i]);
//			}
//		}
//		public Token(Enum type, string symbol)
//		{
//            Type = type;
//			Symbol = symbol;
//		}
//        /// <summary>
//        /// Root token.
//        /// </summary>
//        /// <param name="symbol">The whole parsed text.</param>
//		public Token(string symbol)
//		{
//            Type = default;
//			Symbol = symbol;
//		}
//		public Token GoFirst(Token token)
//		{
//            Token first = this;
//            while (first.left != null)
//            {
//                first = first.left;
//            }
//            return this;
//		}
//		public Token GoLast(Token token)
//		{
//            Token last = this;
//            while (last.right != null)
//            {
//                last = last.right;
//            }
//            return this;
//		}
//        public Token GoLeft(int count)
//        {
//            Token token = this;
//            while (count-- > 0)
//            {
//                token = token.left;
//                if (token == null)
//                {
//                    throw new Exception("No more tokens left on the left.");
//                }
//            }
//            return token;
//        }
//        public Token GoRight(int count)
//        {
//            Token token = this;
//            while (count-- > 0)
//            {
//                token = token.right;
//                if (token == null)
//                {
//                    throw new Exception("No more tokens left on the right.");
//                }
//            }
//            return token;
//        }
//		public Token AddDown(Token token)
//		{
//			if (down != null)
//			{
//                throw new Exception("Cannot add a child because there already is one.");
//			}

//            token.up = this;
//			down = token;
//            return this;
//		}
//		public Token AddRight(Token token)
//		{
//			if (token == null)
//			{
//				return null;
//			}

//			// Set token
//			token.up = up;
//			token.left = this;
//			token.right = right;
//			// Set chain
//			if (right != null)
//			{
//				right.left = token;
//			}
//			right = token;

//            return this;
//		}
//		public Token AddLeft(Token token)
//		{
//			if (token == null)
//			{
//				return null;
//			}

//			// Set token
//			token.up = up;
//			token.right = this;
//			token.left = left;
//			// Set chain
//			if (left != null)
//			{
//				left.right = token;
//			}
//			left = token;

//            return this;
//		}
//		public Token AddFirst(Token token)
//		{
//            Token first = this;
//            while (first.left != null)
//            {
//                first = first.left;
//            }
//            AddFirst(token);
//            return this;
//		}
//		public Token AddLast(Token token)
//		{
//            Token last = this;
//            while (last.right != null)
//            {
//                last = last.right;
//            }
//            AddRight(token);
//            return this;
//		}
//        public Token Replace(Token token)
//        {
//            // Set the new one
//			token.up = up;
//			token.right = right;
//			token.left = left;
//            // Reset the old one
//            up = null;
//            right = null;
//            left = null;

//            return token;
//        }
//        public Token MoveLeft(Token token)
//        {
//            // Set the new one
//			token.up = up;
//			token.right = right;
//			token.left = left;
//            // Reset the old one
//            up = null;
//            right = null;
//            left = null;

//            return token;
//        }
//		public Token InsertSelf(List<Token> param_tokens)
//		{
//			if (param_tokens.Count == 0)
//			{
//				return this;
//			}
//			if (param_tokens.Count == 1)
//			{
//				SwitchSelf(param_tokens[0]);
//				param_tokens[0].down = this;
//				up = param_tokens[0];
//				return param_tokens[0];
//			}

//			//string a = param_token.ToString();

//			Token tokenLeft = param_tokens[0];
//			Token tokenRight = param_tokens[param_tokens.Count - 1];
//			//for (int i = 0; i < param_count - 1; i++)
//			//{
//			//	tokenRight = tokenRight.Next;
//			//}

//			right = tokenRight.right;
//			left = tokenLeft.left;
//			up = tokenLeft.up;
//			down = tokenLeft;

//			tokenLeft.left = null;
//			tokenRight.right = null;
//			{
//				Token tempToken = tokenLeft;
//				while (tempToken != null)
//				{
//					if (tempToken.up.down == tempToken)
//					{
//						tempToken.up.down = this;
//					}
//					tempToken.up = this;
//					tempToken = tempToken.right;
//				}
//			}

//			foreach (Token token in param_tokens)
//			{
//				down.Remove(token, SearchDirection.RIGHT);
//			}

//			//string b = param_token.ToString();
//			string c = ToString();

//			return this;
//		}
//        //public void Replace(Enum type)
//        //{
//        //    Type = type;
//        //}
//		public Token InsertSelf(Token param_token, int param_count)
//		{
//			string a = param_token.ToString();

//			Token tokenLeft = param_token;
//			Token tokenRight = param_token;
//			for (int i = 0; i < param_count - 1; i++)
//			{
//				tokenRight = tokenRight.right;
//			}

//			right = tokenRight.right;
//			left = tokenLeft.left;
//			up = tokenLeft.up;
//			down = tokenLeft;

//			tokenLeft.left = null;
//			tokenRight.right = null;
//			{
//				Token tempToken = tokenLeft;
//				while (tempToken != null)
//				{
//					if (tempToken.up.down == tempToken)
//					{
//						tempToken.up.down = this;
//					}
//					tempToken.up = this;
//					tempToken = tempToken.right;
//				}
//			}

//			string b = param_token.ToString();
//			string c = ToString();

//			return this;
//		}
//		public Token SwitchSelf(Token param_token)
//		{
//			Token tempNext = right;
//			Token tempPrevious = left;
//			Token tempParent = up;
//			Token tempChild = down;

//			right = param_token.right;
//			left = param_token.left;
//			up = param_token.up;
//			down = param_token.down;

//			if (left != null)
//			{
//				left.right = right;
//			}
//			if (right != null)
//			{
//				right.left = left;
//			}
//			if (up != null && up.down == this)
//			{
//				up.down = this;
//			}
//			if (down != null)
//			{
//				down.up = this;

//				Token temppChild = down;
//				while (temppChild.right != null)
//				{
//					temppChild.up = this;
//					temppChild = temppChild.right;
//				}
//			}

//			param_token.right = tempNext;
//			param_token.left = tempPrevious;
//			param_token.up = tempParent;
//			param_token.down = tempChild;

//			if (param_token.left != null)
//			{
//				param_token.left.right = param_token.right;
//			}
//			if (param_token.right != null)
//			{
//				param_token.right.left = param_token.left;
//			}
//			if (param_token.up != null && param_token.up.down == this)
//			{
//				param_token.up.down = this;
//			}
//			if (param_token.down != null)
//			{
//				param_token.down.up = this;

//				Token temppChild = param_token.down;
//				while (temppChild.right != null)
//				{
//					temppChild.up = this;
//					temppChild = temppChild.right;
//				}
//			}

//			return param_token;
//		}
//		public void RemoveSelf()
//		{
//			while (down != null)
//			{
//				down.RemoveSelf();
//			}
//			if (left != null)
//			{
//				left.right = right;
//			}
//			if (right != null)
//			{
//				right.left = left;
//			}
//			// If first in the line -- set parent's pointer to the next in the line
//			if (up != null && up.down == this)
//			{
//				up.down = right;
//			}
//		}
//		public void Remove(Token param_token, SearchDirection param_searchDirection)
//		{
//			// Check if found
//			if (param_token == this)
//			{
//				RemoveSelf();
//				return;
//			}
//			// Move further
//			if (right != null && (param_searchDirection & SearchDirection.RIGHT) != 0)
//			{
//				right.Remove(param_token, param_searchDirection);
//			}
//			if (left != null && (param_searchDirection & SearchDirection.LEFT) != 0)
//			{
//				left.Remove(param_token, param_searchDirection);
//			}
//			if (up != null && (param_searchDirection & SearchDirection.UP) != 0)
//			{
//				up.Remove(param_token, param_searchDirection);
//			}
//			if (down != null && (param_searchDirection & SearchDirection.DOWN) != 0)
//			{
//				down.Remove(param_token, param_searchDirection);
//			}

//		}
//		public List<Token> ToList()
//		{
//			List<Token> tokens = new List<Token>();
//			ToListRecursion(this, tokens);
//			return tokens;
//		}
//		public static List<Token> ToList(Token param_token)
//		{
//			List<Token> tokens = new List<Token>();
//			ToListRecursion(param_token, tokens);
//			return tokens;
//		}
//		private static void ToListRecursion(Token param_token, List<Token> param_tokens)
//		{
//			param_tokens.Add(param_token);
//			if (param_token.down != null)
//			{
//				ToListRecursion(param_token.down, param_tokens);
//			}
//			if (param_token.right != null)
//			{
//				ToListRecursion(param_token.right, param_tokens);
//			}
//		}
//		public string ToString(bool param_recursive)
//		{
//			if (Type == TokenType.ROOT)
//			{
//				return down != null ? down.ToString(true) : "";
//			}
//			else if (param_recursive)
//			{
//				return Symbol + (down != null ? down.ToString(true) : "") + (right != null ? right.ToString(true) : "");
//			}
//			else
//			{
//				return Symbol;
//			}
//		}
//		public string Print()
//		{
//			StringBuilder stringBuilder = new StringBuilder();

//			stringBuilder.Append(String.IsNullOrEmpty(Symbol) ? typeof(T).ToString() : Symbol);

//			Token child = down;
//			if (child != null)
//			{
//				stringBuilder.Append("( ");
//				while (child != null)
//				{
//					//stringBuilder.Append(String.IsNullOrEmpty(child.Symbol) ? child.Type.ToString() : child.Symbol);
//					stringBuilder.Append(child.Print());
//					child = child.right;
//					if (child != null)
//					{
//						stringBuilder.Append(" ");
//					}
//				}
//				stringBuilder.Append(" )");
//			}
//			return stringBuilder.ToString();
//		}
//		public override string ToString()
//		{
//			if (down == null)
//			{
//				return typeof(T).ToString();
//			}

//			//if (Child != null)
//			//{
//				StringBuilder stringBuilder = new StringBuilder();

//				stringBuilder.Append(typeof(T));

//				Token child = down;
//				stringBuilder.Append("( ");
//				while (child != null)
//				{
//					stringBuilder.Append(child);
//					child = child.right;
//					if (child != null)
//					{
//						stringBuilder.Append(", ");
//					}
//				}
//				stringBuilder.Append(" )");
//				return stringBuilder.ToString();
//			//}
//			//return Type.ToString() + (Child != null ? ("( " + Child.ToString() + " )") : "") + (Next != null ? (", " + Next.ToString()) : "") ;
//			//return ToString(false);
//		}
//	}
//	public enum TokenType : int
//	{
//		ROOT = 1,
//		COMMENT,
//		RETURN,
//		NEWLINE,

//		BRACKET_SQUARE,
//		BRA_SQUARE,
//		KET_SQUARE,
//		BRACKET_ROUND,
//		BRA_ROUND,
//		KET_ROUND,
//		BRACKET_FIGURED,
//		BRA_FIGURED,
//		KET_FIGURED,
//		BRACKET_SHARP,
//		BRA_SHARP,
//		KET_SHARP,
//		BRA_0,
//		KET_0,
//		BRACKET_0,
//		BRA_1,
//		KET_1,
//		BRACKET_1,
//		BRA_2,
//		KET_2,
//		BRACKET_2,
//		BRA_3,
//		KET_3,
//		BRACKET_3,
//		BRA_4,
//		KET_4,
//		BRACKET_4,
//		BRA_5,
//		KET_5,
//		BRACKET_5,
//		BRA_6,
//		KET_6,
//		BRACKET_6,
//		BRA_7,
//		KET_7,
//		BRACKET_7,
//		BRA_8,
//		KET_8,
//		BRACKET_8,
//		BRA_9,
//		KET_9,
//		BRACKET_9,

//		TAB,
//		SPACE,
//		LETTER,
//		DIGIT,
//		STRING,
//		CHARACTER,
//		WORD,
//		NUMBER,
//		NUMBER_HEX,
//		NUMBER_OCT,
//		NUMBER_DEC,
//		INTEGER,
//		REAL,
//		TIME,
//		DATE,

//		VARIABLE,
//		VALUE,
//		NAME,
//		LITERAL,

//		AND,
//		OR,
//		NOT,
//		XOR,
//		INCREMENT,
//		DECREMENT,

//		PLUS,
//		MINUS,
//		ASTERISK,
//		SLASH,
//		EQUAL,
//		NOT_EQUAL,
//		LESS,
//		LESS_OR_EQUAL,
//		NOT_LESS,
//		GREATER,
//		GREATER_OR_EQUAL,
//		NOT_GREATER,
//		ASSIGN,

//		EXCLAMATION,
//		QUESTION,
//		COMMA,
//		DOT,
//		PERIOD,
//		DASH,
//		SINGLE_QUOTE,
//		DOUBLE_QUOTE,
//		COLON,
//		SEMICOLON,

//		UNDERLINE,
//		UNDERSCORE,
//		AT,
//		SHARP,
//		DOLLAR,
//		PERCENTAGE,
//		CIRCUMFLEX,
//		BAR,
//		AMPERSAND,
//		BACKSLASH,
//		GRAVE,
//		TILDE,

//		CUSTOM_0,
//		CUSTOM_1,
//		CUSTOM_2,
//		CUSTOM_3,
//		CUSTOM_4,
//		CUSTOM_5,
//		CUSTOM_6,
//		CUSTOM_7,
//		CUSTOM_8,
//		CUSTOM_9,

//		ONE,
//		TWO,
//		THREE,
//		MULTIPLE,
//		ANY,
//		UNKNOWN = 0
//	}
//}
